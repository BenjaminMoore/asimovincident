﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour
{

    public void PauseMovement(float a_dur)
    {
        if(a_dur == 1)
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().PauseMovement(true);
        else
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().PauseMovement(false);

    }
}
