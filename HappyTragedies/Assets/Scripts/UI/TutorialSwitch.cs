﻿using UnityEngine;
using System.Collections;

public class TutorialSwitch : MonoBehaviour
{
    public GameObject[] m_controls;
    PlayerController player;
    int oldInput = 0;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.m_inputDevice != oldInput)
        {
            if (player.m_inputDevice == 1)
            {
                m_controls[0].SetActive(true);
                m_controls[1].SetActive(false);
            }
            else if (player.m_inputDevice == 2)
            {
                m_controls[0].SetActive(false);
                m_controls[1].SetActive(true);
            }
        }
    }
}
