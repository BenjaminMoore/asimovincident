﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{
    public Image LeftBar;
    public Image RightBar;

    Health BossHealth;

    // Use this for initialization
    void Start()
    {
        BossHealth = GameObject.FindGameObjectWithTag("Boss").GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        LeftBar.fillAmount = BossHealth.GetHealth() / BossHealth.GetMaxHealth();
        RightBar.fillAmount = BossHealth.GetHealth() / BossHealth.GetMaxHealth();
    }
}
