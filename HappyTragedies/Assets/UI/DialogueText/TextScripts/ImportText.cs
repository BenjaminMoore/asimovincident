﻿using UnityEngine;
using System.Collections;

public class ImportText : MonoBehaviour {

	public TextAsset textFile;
	public string[] textLines;

	// Use this for initialization
	void Awake () 
	{
		if (textFile != null) 
		{
			textLines = (textFile.text.Split ('\n'));
		}

	}

}
