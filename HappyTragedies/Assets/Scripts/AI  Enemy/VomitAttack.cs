﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS 
/// LAST EDITTED: 9th JULY 2017
/// This behaviour is the ranged attack for the Ranged enemies. 
/// It requires references to the player and the object that it will use as a projectile 
/// utilises Unity Animation Events to call functions to break out of the different states of this behaviour. 
/// </summary>
public class VomitAttack : AIBehaviour {

    //==========================
    // Private Vars 
    GameObject          m_ProjectilePrefab;             //Refernce to the unity Prefab for the object spawned as a projectile
    ParticleSystem      m_PrepParticles;                //Particles to play when the attack is started
    EnemyAI             m_AI;                           //Reference to the AI object that owns this behaviour
    GameObject          m_Player;                       //A reference to the player object
    float               m_PrepTime = 3.0f;              //The ammount of time it takes to prepare the ranged attack
    float               m_speedFactor = 0.0f;           //Used to scale the speed of projectiles
    eResult             m_CurrentState = eResult.Fail;  //Used yo ytack the current state of the behaviour FAIL = Not Initiated | SUCCESS = FINISHED
                                                        //===================


    public override eResult Initialise()
    {
        //Gather any required variables and references 
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_ProjectilePrefab == null)
            m_ProjectilePrefab = m_AI.GetProjectilePrefab();
        if (m_PrepParticles == null)
            m_PrepParticles = m_AI.GetRangedParticles();
        if (m_speedFactor == 0.0f)
            m_speedFactor = m_AI.GetSpeedFactor();

        if (m_Player && m_AI && m_ProjectilePrefab && m_PrepParticles && m_speedFactor != 0.0f)
            return eResult.Success;
        else return eResult.Fail; 
    }


    //Used to begin and update the behaviour for the duration of its lifespan 
    public override eResult Execute ()
    {
        if (m_CurrentState == eResult.Fail)         //If not initiated
        {       
            m_CurrentState = eResult.InProgress;    //update state
            if (m_PrepParticles != null)            //play particle effects
                m_PrepParticles.Play();
            m_AI.SetAnimationState(5);              //Play the VomitAttack Animation (State 5 on Animator) 
            transform.LookAt(m_Player.transform.position);                          //Make the Enemies lookat the player
            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false; //Disable the movement controller for the object
        }
        else if (m_CurrentState == eResult.InProgress)      //We already in progress
        {
            transform.LookAt(m_Player.transform.position);  //MMake the Enemy lookat the player 
        }
        return m_CurrentState;
	}
	

    //Used to determine if an attack is possible 
	public override eResult Evaluate ()
    {
        //Default the CurrentState to Fail so Execute knows to begin
        m_CurrentState = eResult.Fail; 

            //figure out if the AI is able to attack
            if (m_ProjectilePrefab != null && m_Player != null) //if we have a player to hit and a bullet to hit with
            {
                RaycastHit rayHit;
                //if (Physics.Raycast(transform.position, m_Player.transform.position - transform.position, out rayHit))
                if (Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), Vector3.Normalize(m_Player.transform.position - transform.position), out rayHit))
                {
                    if (rayHit.collider.tag == "Player" && rayHit.distance <= m_AI.GetAttackRange() && rayHit.distance >= 2) //if we can see the player && they're in range
                    {
                        //we can attack
                        return eResult.Success; 
                    }
                }
            }
        return eResult.Fail;        
	}

    //Will be used once the ranged attack is animated
    public void FinishedRangeAttack()
    {
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        m_AI.SetAnimationState(0);
        m_CurrentState = eResult.Success; 
    }

    public void SpawnProjectile()
    {
        Vector3 direction = transform.position - m_Player.transform.position;
        GameObject projectile = Instantiate(m_ProjectilePrefab, transform.position + transform.forward + new Vector3(0, 0.5f, 0), transform.rotation) as GameObject;
        //calculate the time we want the projectile to take
        float distance = Vector3.Distance(m_Player.transform.position, transform.position + transform.forward) / 10.0f;
        projectile.GetComponent<Rigidbody>().AddForce(GetProjectileSpeed(distance * m_speedFactor), ForceMode.VelocityChange);
        projectile.GetComponent<EnemySpitProjectile>().setSource(gameObject);
        if (m_PrepParticles != null)
            m_PrepParticles.Stop();
    }

    //calculates the force to apply to the projectile's rigidbody to make it travel in a parabolic arc
    //Code is sourced from unity user Tomer-Barkan's reply in this thread: http://answers.unity3d.com/questions/248788/calculating-ball-trajectory-in-full-3d-world.html
    private Vector3 GetProjectileSpeed(float time)
    {
        Vector3 TargetRoute = m_Player.transform.position - (transform.position + transform.forward + new Vector3(0, 0.5f, 0));
        Vector3 TargetRouteXY = TargetRoute;
        TargetRouteXY.y = 0;

        float y = TargetRoute.y;
        float xz = TargetRouteXY.magnitude;

        float v0y = y / time + 0.5f * Physics.gravity.magnitude * time;
        float v0xz = xz / time;

        Vector3 ProjectileSpeed = TargetRouteXY.normalized;
        ProjectileSpeed *= v0xz;
        ProjectileSpeed.y = v0y;

        return ProjectileSpeed; 
    }
}
