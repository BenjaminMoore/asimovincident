﻿using UnityEngine;
using System.Collections;

public class EnemyHealthBar : MonoBehaviour
{
    Health m_enemyHealth;
    float m_maxSize;


    RectTransform m_canvasTransform;


    bool dirtyFlag;
    // Use this for initialization
    void Start()
    {
        m_enemyHealth = gameObject.GetComponentInParent<Health>();
        if(m_enemyHealth == null)
        {
            dirtyFlag = true;
        }
        else
        {
            dirtyFlag = false;
            m_maxSize = gameObject.GetComponent<RectTransform>().sizeDelta.x;
        }
        RectTransform[] temps = GetComponentsInParent<RectTransform>();
        foreach (RectTransform rt in temps)
        {
            if (rt.tag == "EnemyHealth")
            {
                m_canvasTransform = rt;
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(dirtyFlag)
        {
            m_enemyHealth = gameObject.GetComponentInParent<Health>();
            if (m_enemyHealth == null)
            {
                dirtyFlag = true;
            }
            else
            {
                dirtyFlag = false;
                m_maxSize = gameObject.GetComponent<RectTransform>().sizeDelta.x;
            }
        }
        else
        {

            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(m_maxSize *  (m_enemyHealth.GetHealth() / m_enemyHealth.GetMaxHealth()), gameObject.GetComponent<RectTransform>().sizeDelta.y);

        }

        m_canvasTransform.LookAt(m_canvasTransform.position + Camera.main.transform.forward);
        if(m_enemyHealth.GetHealth() <= 0)
        {
            gameObject.GetComponentInParent<Canvas>().enabled = false;
            //Destroy(m_canvasTransform.gameObject);
        }
    }
}
