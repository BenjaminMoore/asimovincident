﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Health m_playerHealth;

    bool dirtyFlag;
    // Use this for initialization
    void Start()
    {
        m_playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
        if (m_playerHealth == null)
        {
            dirtyFlag = true;
        }
        else
        {
            dirtyFlag = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (dirtyFlag)
        {
            m_playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
            if (m_playerHealth == null)
            {
                dirtyFlag = true;
            }
            else
            {
                dirtyFlag = false;
            }
        }
        else
        {
            GetComponent<Image>().fillAmount = m_playerHealth.GetHealth() / m_playerHealth.GetMaxHealth();
        }
    }


    public void OnLevelWasLoaded(int level)
    {
        m_playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
        if (m_playerHealth == null)
        {
            dirtyFlag = true;
        }
        else
        {
            dirtyFlag = false;
        }
    }

}

