﻿using UnityEngine;
using System.Collections;

public class BossRangedTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            BossAI ai = GetComponentInParent<BossAI>();
            ai.SetPlayerClose("Ranged", true);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            BossAI ai = GetComponentInParent<BossAI>();
            ai.SetPlayerClose("Ranged", false);
        }
    }
}
