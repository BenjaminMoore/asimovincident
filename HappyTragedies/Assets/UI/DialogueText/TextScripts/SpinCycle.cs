﻿using UnityEngine;
using System.Collections;

public class SpinCycle : MonoBehaviour {

	public float degreesPerSecond;
	public bool canSpin;

	void Start () {
		canSpin = false;
	}

	void Update () {
		if (canSpin == true) 
		{
			transform.Rotate (new Vector3 (0, 0, degreesPerSecond) * Time.deltaTime);
		}
	}
}
