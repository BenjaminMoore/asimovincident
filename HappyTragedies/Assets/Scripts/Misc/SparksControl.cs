﻿using UnityEngine;
using System.Collections;

public class SparksControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<ParticleSystem>().Clear();
        GetComponent<ParticleSystem>().Play();

	}
	
	// Update is called once per frame
	void Update () {

        if (!GetComponent<ParticleSystem>().IsAlive())
            Destroy(gameObject);
    }
}
