﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS 
/// Last Editted: 9/7/2017 by Douglas Landers 
/// Contact attack is used as a melee attack for the Grunt Enemies, 
/// it will deal damage upon collision with the player and will then play its animation (as to the designers request, this is not a bug), 
/// HeadButtAttack refers to the melee attack that will deal damage during its animation
/// </summary>
public class ContactAttack : AIBehaviour {

    //===================
    //Vars 
    EnemyAI                         m_AI;           //Reference to the AI component
    PlayerController                m_Player;       //Reference to the player gameobject
    UnityEngine.AI.NavMeshAgent     m_Agent;        //Reference to the AIs NavMeshAgent
    Rigidbody                       m_rigidbody;    //Reference to the AIs rigidbody

    eResult                         m_CurrentState = eResult.Fail; //Used to track the current phase of the behaviour
    //====================

    //Gather any references and variables required for this script to run
    public override eResult Initialise()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        if (m_rigidbody == null)
            m_rigidbody = GetComponent<Rigidbody>();

        if (m_AI && m_Agent && m_Player && m_rigidbody)
            return eResult.Success;
        else return eResult.Fail;
    }

    //Will determine if the player is close enough to attack,
    //then returns whether or not the behaviour should/can be executed.
    public override eResult Evaluate()
    {
        m_CurrentState = eResult.Fail; 
        
        if (m_AI.IsPlayerClose())
            return eResult.Success;
        else return eResult.Fail; 
    }

    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail && m_AI && m_Player) //If the behaviour hasn't been started yet, and our references are valid (not null) 
        {
            m_Player.DoDamage(m_AI.m_BaseDamage); //Damage the player
            m_AI.SetAnimationState(2); //play the appropriate animation
            m_AI.ResetAttackCounter();
            m_Agent.enabled = false; //turn off the navmesh agent so the AI cannot move while attacking
            m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation; //prevent the AI from rotating while attacking
            m_CurrentState = eResult.InProgress; //update our current state
        }
        return m_CurrentState; //return our current state so ENemyAI knows whether or not to continue this behaviour next frame 
    }

    public void InflictDamage()
    {
        //empty function, is only here so the animation event has a reciever. 
        //This function is used in HeadButtAttack.cs
    }

    //Makes sure the AI can return to normal behaviour
    public void FinishHeadbuttAttack()
    {
        m_AI.SetAnimationState(0); //return to idle animation
        m_AI.SetActionState(0); //set the action state to idle
        m_Agent.enabled = true; //reactivate our navmeshagent
        m_rigidbody.constraints = RigidbodyConstraints.None; //allow the rigidbody to rotate again 
        m_CurrentState = eResult.Success; 
    }


}
