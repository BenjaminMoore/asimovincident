﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour
{


    public enum STATS
    {
        SPEEDMULTIPLIER = 0,
        MAXHEALTHMULTIPLIER = 1,
        REGENMULTIPLIER = 2,
        XPMULTIPLIER = 3,
    }

    public int m_StaminaUpgradeLvl;
    public int m_maxHealthUpgradeLvl;
    public int m_healingUpgradeLvl;
    public int m_XPBonusUpgradeLvl;

    public float m_StaminaIncreasePerLvlBoost;
    public float m_StaminaRatePerLvlBoost;

    public float m_maxHealthPerLvlBoost;
    public float m_healingPerLvlBoost;
    public float m_XPBonusPerLvlBoost;

    public float m_MaxHealthLevels;
    public float m_MaxStaminaLevels;
    public float m_MaxXPLevels;
    public float m_MaxHealingLevels;

    public ParticleSystem m_StatUpgradeParticles; 

    bool dirty = true;

    //UIStats Handle
    private UIStats m_UIStats;


    // Use this for initialization
    void Awake()
    {
        m_StaminaUpgradeLvl = 0;
        m_maxHealthUpgradeLvl = 0;
        m_healingUpgradeLvl = 0;
        m_XPBonusUpgradeLvl = 0;
        if (GameObject.FindGameObjectWithTag("GameManager"))
        {
            m_UIStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetUIStats();

            if (!m_UIStats)
                dirty = true;
            else
                dirty = false;

            if (!dirty)
                UpdatePlayerStats();
        }
             
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectWithTag("GameManager"))
        {
            if (dirty)
                m_UIStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetUIStats();

            if (!m_UIStats)
                dirty = true;
            else
                dirty = false;

            if (!dirty)
                UpdatePlayerStats();
        }
    }

    public void UpgradeStat(STATS a_stat, int a_level)
    {
        //a_percentage /= 100;
        switch(a_stat)
        {
            case STATS.SPEEDMULTIPLIER:
                m_StaminaUpgradeLvl += a_level;
                if (m_StatUpgradeParticles != null) PlayParticles(); 
                break;
            case STATS.MAXHEALTHMULTIPLIER:
                m_maxHealthUpgradeLvl += a_level;
                if (m_StatUpgradeParticles != null) PlayParticles();
                //gameObject.GetComponent<Health>().SetMaxHealth(gameObject.GetComponent<Health>().GetMaxHealth() + (m_maxHealthUpgradeLvl * m_maxHealthPerLvlBoost));
                break;
            case STATS.REGENMULTIPLIER:
                m_healingUpgradeLvl += a_level;
                if (m_StatUpgradeParticles != null) PlayParticles();
                break;
            case STATS.XPMULTIPLIER:
                m_XPBonusUpgradeLvl += a_level;
                if (m_StatUpgradeParticles != null) PlayParticles();
                break;
            default:
                break;
        }
        //Play the particle effect 

        if (GameObject.FindGameObjectWithTag("GameManager"))
        {
            if (dirty)
                m_UIStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetUIStats();

            if (!m_UIStats)
                dirty = true;
            else
                dirty = false;

            if (!dirty)
                UpdatePlayerStats();
        }
    }

    //used for checking if we can still upgrade the desired skill 
    public bool IsStatMaxed(STATS a_stat)
    {
        switch(a_stat)
        {
            case STATS.SPEEDMULTIPLIER:
                if (m_StaminaUpgradeLvl == m_MaxStaminaLevels)
                    return true;
                else return false; 
            case STATS.MAXHEALTHMULTIPLIER:
                if (m_maxHealthUpgradeLvl == m_MaxHealthLevels)
                    return true;
                else return false; 
            case STATS.REGENMULTIPLIER:
                if (m_healingUpgradeLvl == m_MaxHealingLevels)
                    return true;
                else return false; 
            case STATS.XPMULTIPLIER:
                if (m_XPBonusUpgradeLvl == m_MaxXPLevels)
                    return true;
                else return false; 
        }
        return true; 
    }

    public float GetStaminaBuff()
    {
        return m_StaminaIncreasePerLvlBoost * (m_StaminaUpgradeLvl);
    }
    public float GetStaminaRateBuff()
    {
        return m_StaminaRatePerLvlBoost * (m_StaminaUpgradeLvl);
    }
    public float GetMaxHealthBuff()
    {
        return m_maxHealthPerLvlBoost * (m_maxHealthUpgradeLvl);
    }
    public float GetXPBuff()
    {
        return 1 + (m_XPBonusPerLvlBoost * (m_XPBonusUpgradeLvl));
    }
    public float GetRegenBuff()
    {
        return (m_healingPerLvlBoost * (m_healingUpgradeLvl));
    }

    void UpdatePlayerStats()
    {
        m_UIStats.UpdateMaxHealth(m_maxHealthUpgradeLvl);
        m_UIStats.UpdateMvSpd(m_StaminaUpgradeLvl);
        m_UIStats.UpdateHPRegen(m_healingUpgradeLvl);
        m_UIStats.UpdateXPBonus(m_XPBonusUpgradeLvl);
    }

    void PlayParticles()
    {
        m_StatUpgradeParticles.Play(); 
    }
}
