﻿using UnityEngine;
using System.Collections;

public class AlphaSpawnExit : MonoBehaviour
{

    public GameObject m_TeleportPrefab; 

    public void CreateExit()
    {
        GameObject telefab = GameObject.Instantiate(m_TeleportPrefab, transform.parent.position, Quaternion.identity) as GameObject; 
    }
}
