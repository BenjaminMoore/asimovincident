﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Document Owner Benjamin Moore
 Created 20/6/2017
 This Class Is responsible for generating all the weapons and armor that will populate a  shop
 It is related to the ShopFunction class which purchases the items created here
 It uses the weaponGenerator Class to create the weapons
 */

public class GenerateStoreContents : MonoBehaviour {

    //cache weapengenerator
    private WeaponGenerator weapGen;
    private ManageShop shop;
    //array to store weapons to be made and associated costs
    GameObject[] weaponsToBeGenerated;
    int[] weapCost;

    //variables to alter cost outcomes, accessible to designers to tweak values
    public int baseCost;
    public int varience;
    public int uncommonIncrease;
    public int rareIncrease;
    public int epicIncrease;
    public int legendaryIncrease;



    //In start get a weapongenerator to create the weapons
    //call the create contents, which has sub function to populate the shop


    void Start() {
        weapGen = GameManager.instance.GetComponent<WeaponGenerator>();
        weaponsToBeGenerated = new GameObject[3];
        weapCost = new int[3];
        shop = GetComponent<ManageShop>();
        CreateContents();
        shop.setCostUI();
        shop.setNameUI();
    }


    //calls all the functions required to populate the shop
    void CreateContents()
    {
        CreateWeapons();
        GenerateWeaponCosts();
        OrderWeapons();
        DeactivateWeapons();
    }

    //creates weapons, uses weapon generator class
    //allows only the creation of 1 legendary
    void CreateWeapons()
    {
        bool legendaryMade = false;
        int currentLevel = GameManager.instance.m_currentLevel + 26;
        for(int i = 0; i< 3; i++)
        {
            GameObject weaponGenerated = weapGen.GenerateWeapon(100);
            //move weapons to spread them out in front of shop
            Vector3 Move = new Vector3(i-1, 0, -2);
            weaponGenerated.transform.position = transform.position;
            weaponGenerated.transform.position += Move;
            weaponsToBeGenerated[i] = weaponGenerated;
            if (weaponsToBeGenerated[i].GetComponent<Weapon>().m_Rarity == Weapon.Rarity.LEGENDARY)
            {
                if (legendaryMade == false)
                { legendaryMade = true; }
                else { i--; }
            }

        }
    }

    void GenerateWeaponCosts()
    {
        for (int i = 0; i < 3; i++)
        {
            weapCost[i] = baseCost;
            switch (weaponsToBeGenerated[i].GetComponent<Weapon>().m_Rarity)
            {
                case Weapon.Rarity.LEGENDARY:
                    weapCost[i] += legendaryIncrease;
                    break;

                case Weapon.Rarity.EPIC:
                    weapCost[i] += epicIncrease;
                    break;

                case Weapon.Rarity.RARE:
                    weapCost[i] += rareIncrease;
                    break;

                case Weapon.Rarity.UNCOMMON:
                    weapCost[i] += uncommonIncrease;
                    break;

                default:
                    break;
            }

            int randVariance = Random.Range(0, varience);
            weapCost[i] += randVariance;
        }
    }

    void OrderWeapons()
    {

        int[] vals = new int[3];           
        for(int i = 0; i< 3; i++)
        {
            vals[i] = weapCost[i];
        }

        //sort vals
        int lowestVal = 0;
        int highestVal = 0;
        int lowestIndex = 0;
        int highestIndex = 0;
        int midVal = 0;
        int midIndex = 0;
        for(int i = 0; i <3; i++)
        {
            if(vals[i] < lowestVal || lowestVal == 0)
            {
                lowestVal = vals[i];
                lowestIndex = i;
            }
        }
        for (int i = 0; i < 3; i++)
        {
            if (vals[i] > highestVal || lowestVal == 0)
            {
                highestVal = vals[i];
                highestIndex = i;
            }
        }
        for (int i = 0; i < 3; i++)
        {
            if (i != lowestIndex && i != highestIndex)
            {
                midVal = vals[i];
                midIndex = i;
            }
        }
        GameObject[] tempWeps = new GameObject[3];
        tempWeps[0] = weaponsToBeGenerated[0];
        tempWeps[1] = weaponsToBeGenerated[1];
        tempWeps[2] = weaponsToBeGenerated[2];

        weaponsToBeGenerated[0] = tempWeps[lowestIndex];
        weaponsToBeGenerated[1] = tempWeps[midIndex];
        weaponsToBeGenerated[2] = tempWeps[highestIndex];

        weapCost[0] = lowestVal;
        weapCost[1] = midVal;
        weapCost[2] = highestVal;
    }

    void CreateArmor()
    {
       //ToDo create the armor items
    }

    void GenerateArmorCosts()
    {

    }

    void OrderArmor()
    {

    }

    public GameObject GetWeapon(int a_weapNum)
    {
        switch (a_weapNum)
        {
            case 0:
                return weaponsToBeGenerated[0];
            case 1:
                return weaponsToBeGenerated[1];
            case 2:
                return weaponsToBeGenerated[2];
            default:
                Debug.Log("Unexpected switch statement: GetWeapon Returned NULL");
                return null;
        }
    }

    public int GetWeapCost(int index)
    {
        return weapCost[index];
    }

    void DeactivateWeapons()
    {
        weaponsToBeGenerated[0].SetActive(false);
        weaponsToBeGenerated[1].SetActive(false);
        weaponsToBeGenerated[2].SetActive(false);
    }
}
