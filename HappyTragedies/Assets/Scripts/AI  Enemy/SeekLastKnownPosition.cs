﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Author: Douglas Landers
/// Last Editted: 9/7/2017 by Douglas Landers 
/// This behaviour is used to make the enemy seek to the last position they saw the player at, 
/// is called if the line of sight to the player is broken 
/// </summary>
public class SeekLastKnownPosition : AIBehaviour
{
    //================
    //Private Vars 
    EnemyAI                     m_AI;       //A reference to the EnemyAI component that owns this behaviour 
    UnityEngine.AI.NavMeshAgent m_Agent;    //A reference to the NavMeshAgent used to steer this gameobject. 
    //===================

    //Used to gather any references required for this behaviour to execute. 
    public override eResult Initialise()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (m_AI && m_Agent)
            return eResult.Success;
        else return eResult.Fail; 
    }

    public override eResult Execute()
    {
        //Get the Last Known Position (lkp) and set it as the destination
        Vector3 lkp = m_AI.GetLastKnownPlayerPosition();
        if (lkp != Vector3.zero) //if the LKP is a valid position seek to it and return success
        {
            m_Agent.SetDestination(lkp);    //Set Destination on the NavMeshAgent 
            m_AI.SetActionState(1);         //Update the AIs action state
            m_AI.SetAnimationState(1);      //Play the walking animation (state 1 on animator) 
            return eResult.Success;
        }
        else return eResult.Fail;
        }

    public override eResult Evaluate()
    {
        //success by default, this behaviour has no requirements
        return eResult.Success; 
    }

}
