﻿using UnityEngine;
using System.Collections;

public class BossSpawnEnemiesHelper : MonoBehaviour {

    public GameObject Spawner1;
    public GameObject Spawner2;
    public GameObject Spawner3;
    public GameObject Spawner4;

    public int SpawnCount;
}
