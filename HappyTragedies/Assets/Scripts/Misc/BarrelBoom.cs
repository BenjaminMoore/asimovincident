﻿using UnityEngine;
using System.Collections;

public class BarrelBoom : MonoBehaviour
{
    public float m_explosiveDmg;
    public float m_explosiveRadius;
    public bool m_hurtPlayer;
    public bool m_hurtEnemy;
    public bool m_enemiesCanTrigger;
    public bool m_playerCanTrigger;
    [SerializeField]
    bool m_exploded = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void Explode()
    {
        if (m_exploded)
            return;
        //Get all colliders within a certain radius
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, m_explosiveRadius);
        foreach (Collider col in hitColliders)
        {
            //deal damage to appropriate objects
            if(col.tag == "Player" && m_hurtPlayer)
            {
                col.gameObject.GetComponent<PlayerController>().DoDamage(m_explosiveDmg);
                if (!m_hurtEnemy)
                    break;
            }
            else if (col.tag == "Enemy" && m_hurtEnemy)
                col.gameObject.GetComponent<Health>().DoDamage(m_explosiveDmg);
        }
        m_exploded = true;
        //todo: change model to exploded model

    }

    void OnCollisionEnter(Collision a_other)
    {
        if ((a_other.collider.tag == "Bullet" && m_playerCanTrigger) || (a_other.collider.tag == "EnemyBullet" && m_enemiesCanTrigger))
        {
            Explode();
        }
    }
    void OnParticleCollision(GameObject other)
    {
        if (other.tag == "Bullet" && m_playerCanTrigger)
            Explode();

    }
}
