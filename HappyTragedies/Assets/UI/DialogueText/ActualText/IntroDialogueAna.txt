You made it. Is it online?
Good. I'll take it from here. 
Get Harkinson out to safety.
Takahashi, there's no way to talk me out of this.
I've spent years giving the best friend I've ever had a new life.
Betrayal is not something I intend to repeat.
Now get going.
Thank you Takahashi. Your concern I appreciate but your incompliance I do not. 
Get Harkinson in an escape pod right this moment and go, that is your priority. 
I will not ask again.
Go!
Go.