﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: Douglas Landers 
/// Last Editted: 9/7/2017 by Douglas Landers 
/// This is the behaviour used to position enemy objects correctly for them 
/// to begin their melee attacks, It checks to move the AI to the right position and rotation, 
/// can only be executed if the player can be seen with a raycast. 
/// </summary>
public class MeleeAttackPrepare : AIBehaviour {


    //===============
    //Private Vars 
    GameObject m_Player;                    //A reference to the players gameobject
    UnityEngine.AI.NavMeshAgent m_Agent;    //A reference to the AI's navmeshagent
    EnemyAI m_AI;                           //A reference to the AI's EnemyAI component
    //===============

    //Used to collect any references or variables required for the behaviour 
    public override eResult Initialise()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (m_AI && m_Player && m_Agent)
            return eResult.Success;
        else return eResult.Fail; 
    }

    public override eResult Execute()
    {
        //if we have valid references execute the code
        if (m_Player && m_Agent && m_AI)
        {
            //Check to see if we're far away from the player and need to seek towards them, or if we just need to rotate to get them in the attack zone 
            //Start by getting the distance between the player and the ai 
            float dist = Vector3.Distance(transform.position, m_Player.transform.position);

            if (dist <= m_Agent.stoppingDistance) //if the player is close we just need to rotate
            {
                Quaternion targetRot = Quaternion.LookRotation(m_Player.transform.position - transform.position);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 5 * Time.deltaTime); 
            }
            else
            {
                m_Agent.SetDestination(m_Player.transform.position);  //Set NavMeshAgent destination to player's position
            }
            m_AI.SetActionState(1);
            m_AI.SetAnimationState(1); //set the animation to walking (state 1 on animator) 
            return eResult.Success;
        }
        else
            return eResult.Fail;
    }

    public override eResult Evaluate()
    {
        //only return success if the AI caqn see the player 
        if (m_AI.CanSeePlayer())
            return eResult.Success;
        else return eResult.Fail;
    }

}
