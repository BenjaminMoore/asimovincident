﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: Douglas Landers 
/// Last Editted: 9/7/2017 by Douglas Landers 
/// This hurt behaviour is evaluated whenever an enemy takes damage. It has a % chance of executing which is 
/// a public variable that can be adjusted on the enemyAI component. 
/// If this behaviour is executed, a hurt animation will play and any interactions the enemy was in the middle 
/// of will be purged. 
/// </summary>
public class HurtDefaultBehaviour : AIBehaviour {

    //===============
    //Private Vars 
    EnemyAI m_AI;                           //A reference to the AI component on this game object 
    UnityEngine.AI.NavMeshAgent m_Agent;    //A reference to the NavMeshAgent used to control this object
    Rigidbody m_RigidBody;                  //a refeence to the rigidbody attached to this object. 
    eResult m_State = eResult.Fail;         //The current stage of the behaviour | Fail = not started | In Progress = In Progress | Success = Finished 
    //==============

    //used to gather any required references or variables for this behaviour 
    public override eResult Initialise()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (m_RigidBody == null)
            m_RigidBody = GetComponent<Rigidbody>();

        if (m_AI && m_Agent && m_RigidBody)
            return eResult.Success;
        else return eResult.Fail; 
    }

    //Begins the behaviour by updating the state, clearing inprogress actions and disabling the navmesh agent
    public override eResult Execute()
    {
        if (m_State == eResult.Fail)
        {
            m_State = eResult.InProgress; //Update the state 
            m_AI.PurgeInProgressActions();//Delete any in_progress behaviours 
            m_Agent.enabled = false;      //turn off the Navmeshagent so the enemy can not move until they complete this behaviour
            m_AI.SetAnimationState(4);    //Play the Backwards Jump animation (state 4) 
        }
        return m_State; 
    }

    //Determines whether or not this behaviour should be executed 
    public override eResult Evaluate()
    {
        m_State = eResult.Fail;             //Set the state to its default 

        int Rand = Random.Range(0, 100);    //Get a random number, and compare it to the chance of the behaviour being executed
        if (Rand <= m_AI.GetStunChance())  //if the random number is less than the chance then execute the behaviour
            return eResult.Success;
        else return eResult.Fail; 
    }

    //This function is called by an animation event at the end of the anim 
    public void FinishedHurting()
    {
        m_Agent.enabled = true;                         //Reactivate the Agent 
        m_AI.SetAnimationState(0);                      //Set the animation back to idle (state 0) 
        m_State = eResult.Success;                      //Updae the behaviours state
        m_RigidBody.isKinematic = true;                 //Make sure the enemy is still kinematic 
    }
}
