﻿using UnityEngine;
using System.Collections;

public class BossSpawnEnemies : AIBehaviour {

    //Vars 
    EnemySpawner m_spawner;
    DungeonSection m_currentSection; 
    BossSpawnEnemiesHelper m_HelperClass;
    BossAI m_AI;
    Health m_Health; 

    public override eResult Evaluate()
    {
        if (m_spawner == null)
            m_spawner = GameObject.FindGameObjectWithTag("WTS").GetComponent<EnemySpawner>();
        if (m_AI == null)
            m_AI = GetComponent<BossAI>();
        if (m_Health == null)
            m_Health = GetComponent<Health>(); 
        if (m_currentSection == null)
        {
            DungeonSection[] sections = GameObject.FindGameObjectWithTag("SpawnerManager").GetComponents<DungeonSection>();
            int lvl = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().m_currentLevel + 25;
            foreach (DungeonSection section in sections)
            {
                if (lvl >= section.StartLevel && lvl <= section.EndLevel)
                {
                    m_currentSection = section;
                    break;
                }
            }
        }
        if (m_currentSection != null) //initialise the spawner.
        {
            //find out if this is 50 or 75% 
            if (m_Health.GetHealth() <= m_Health.GetMaxHealth() * 0.50f)
                m_spawner.SetMaxEnemies(m_AI.GetBossFightSpawnRate(1));
            else m_spawner.SetMaxEnemies(m_AI.GetBossFightSpawnRate(0));

            m_spawner.SetRoom(GameObject.FindGameObjectWithTag("BossRoom").GetComponent<Room>());
            m_spawner.SetDamageFactor(m_currentSection.EnemyDamageFactor);
            m_spawner.SetHealthFactor(m_currentSection.EnemyHealthFactor);
            m_spawner.SetRewardFactor(m_currentSection.EnemyRewardFactor);

            m_spawner.SpawnEnemies();

            return eResult.Success; 
        }
        else return eResult.Fail; 
    }

    public override eResult Execute()
    {
        m_spawner.ActivateEnemies();
        if (m_Health.GetHealth() <= m_Health.GetMaxHealth() * 0.50f)
            m_AI.SetHasDone(1);
        else m_AI.SetHasDone(0);
        return eResult.Success; 
    }

}
