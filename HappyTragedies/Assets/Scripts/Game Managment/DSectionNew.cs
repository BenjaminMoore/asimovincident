﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DSectionNew : MonoBehaviour {

    [Tooltip("What level of the dungeon this section starts at")]
    public int StartLevel;
    [Tooltip("What level of the dungeon this section ends at")]
    public int EndLevel;
    [Tooltip("How much to scale enemy damage in this Section")]
    public float EnemyDamageFactor;
    [Tooltip("How much to scale enemy health in this Section")]
    public float EnemyHealthFactor;
    [Tooltip("How much to scale enemy kill rewards in this Section")]
    public float EnemyRewardFactor;
    [Tooltip("How many enemies should spawn in each room in this Section")]
    public float EnemiesPerRoom;
    [Tooltip("How many more enemies should be added each level (multiplier)")]
    public float EnemySpawnIncrease;
    [Tooltip("How much variance in enemy counts room ")]
    public float EnemyCountVariance;
    [Tooltip("The different types of enemies that should spawn in this Section")]
    public GameObject[] EnemyTypes;
    [Tooltip("the different chances for each enemy type being spawned. Should have same number of elements as EnemyTypes")]
    public int[] SpawnChances;
}
