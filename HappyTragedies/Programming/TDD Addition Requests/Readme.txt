This folder hold requests from programmers requesting additions or changes to the technical design documentation

Format for new Classes/Scripts: 

[NAME] 	  (Name of the class/script)
[SUMMARY] (Purpose of the class and what it's role is)
[Variavles] (Type, Name, Privacy and a description)
[Functions] (Return type, Name, Paramters, and a description) 


Format for changes within a class 

[Name] (name of the class/script) 
[New Variables] (type, name, privacy and desription) 
[New Functions] (return type, name, parameters and a description) 
[Misc] 	any changes that should be documented. 