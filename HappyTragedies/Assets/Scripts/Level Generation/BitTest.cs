﻿using UnityEngine;
using System.Collections;

enum Directions
{
    north = 1, south = 2, east = 4, west = 8
}

public class BitTest : MonoBehaviour {
    int iterations;
    int bitTest;
	// Use this for initialization
	void Start () {
        bitTest = 4;
        iterations = 0;
    }
	
	// Update is called once per frame
	void Update () {
        int DoorBools = 0;
        while (iterations < 4)
        {
            int placement=0;

            if (iterations == 0)
            {    placement = (int)Directions.north; }
            if (iterations == 1)
            {  placement = (int)Directions.south; }
            if (iterations == 2)
            {  placement = (int)Directions.east; }
            if (iterations == 3)
            { placement = (int)Directions.west; }

            DoorBools |= placement;
            iterations++;
            if ((DoorBools & 1 << 2) > 0)
            {
            }

        }


    }
}
