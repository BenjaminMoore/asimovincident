﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Author(s): Douglas Landers 
/// Last Editted: 9/7/2017 by Douglas Landers 
/// This behaviour is the seeking behaviour to be attached to ranged enemies. It will determine the 
/// distance the player is from the enemy, if the enemy is close to the player it should seek for a melee attack,
/// otherwise, it should seek to a position it has a clear line of site to attack the player 
/// </summary>
public class SeekForRangedBehaviour : AIBehaviour {

    //======================================
    //Private Vars 
    GameObject                  m_Player;               //A reference to the player gameobject
    bool                        m_MeleeMode = false;    //A bool used to determine if the enemy should seek to melee or ranged attack
    UnityEngine.AI.NavMeshAgent m_Agent;                //A reference to the NavMeshAgent responsible for controlling this AI 
    float                       m_MaxRange = 0f;        //The Max Range that the ranged enemy can attack to
    eResult                     m_CurrentState;         //Used to track the current state of the behaviour. 
    //======================================

    //This function is used to collect any references and variables we need to execute the behaviour
    public override eResult Initialise()
    {
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (m_MaxRange == 0)
            m_MaxRange = GetComponent<EnemyAI>().GetAttackRange();

        if (m_Player && m_Agent && m_MaxRange != 0f)
            return eResult.Success;
        else return eResult.Fail; 
    }

    //Used to execute the functionality of the behaviour 
    public override eResult Execute()
    {
        if (m_MeleeMode) //if the player is close we should seek for a melee attack
        {
            m_Agent.SetDestination(m_Player.transform.position);
            return eResult.Success; 
        }
        else //if the player needs to seek for a ranged attack
        {
            if (m_CurrentState == eResult.Fail)
            {
                m_CurrentState = eResult.InProgress;
                m_Agent.SetDestination(m_Player.transform.position);

                return m_CurrentState;
            }
            else
            {
                //check to see if we can see the player yet
                RaycastHit rayHit;
                if (Physics.Raycast(transform.position, Vector3.Normalize(m_Player.transform.position - transform.position), out rayHit, m_MaxRange))
                {
                    if (rayHit.collider.tag == "Player")
                        return eResult.Success;
                }
                return m_CurrentState;
            }

        }
    }

    //Used to determine if the behaviour should be executed and how it should be executed 
    public override eResult Evaluate()
    {
        m_CurrentState = eResult.Fail; //default our current range.
 
        //perform a raycast, if don't see the player we retrun success so we can position the AI better, otherwuse we don't need to get into position
        RaycastHit rayHit; 
        if (Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), Vector3.Normalize(m_Player.transform.position - transform.position), out rayHit, m_MaxRange) && GetComponent<EnemyAI>().CanSeePlayer())
        {
            if (rayHit.collider.tag != "Player") //If this is true, the enemy cannot see the player and needs to move somewhere they can
            {
                m_MeleeMode = false; 
                return eResult.Success;
            }
            else if (rayHit.collider.tag == "Player" && rayHit.distance <= 2 && !GetComponent<EnemyAI>().IsPlayerClose()) //If this is true the enemy is close to the player and should move in for a melee attack
            {
                m_MeleeMode = true; 
                return eResult.Success; 
            }
        }
        return eResult.Fail;


    }
}
