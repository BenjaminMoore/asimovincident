﻿using UnityEngine;
using System.Collections;

public class GunLevelEffect : MonoBehaviour {

    public float maxScale = 10;
    private float maxSize;
    [Range(1,2)]
    public float speed;
    private float currentSize;
    Material mat;
    private float alphaColour;
    GameObject instance;

	// Use this for initialization
	void Start () {
        MeshFilter activeMesh = GetComponent<MeshFilter>();
        activeMesh.mesh = transform.parent.GetComponent<MeshFilter>().mesh;
        transform.position = transform.parent.transform.position;
        transform.rotation = transform.parent.transform.rotation;
        mat = transform.parent.gameObject.GetComponent<Renderer>().material;
        GetComponent<Renderer>().material = mat;
        mat.SetFloat("_Mode", 3.0f);
        mat.EnableKeyword("_ALPHABLEND_ON");
        maxSize = maxScale + 1;
    }
    void OnEnable()
    {

    }
	
	// Update is called once per frame
	void Update () {
        if((transform.localScale * speed).x > maxSize)
        {
            Vector3 temp;
            temp = new Vector3(maxSize, maxSize, maxSize);
            transform.localScale = temp;
            Destroy(gameObject);
        }
        else
        {
            transform.localScale *= speed;
        }
        Debug.Log("MAX SIZE = " + maxSize);
        Debug.Log("CURRENT SIZE = " + currentSize);
        Debug.Log("ALPHA COLOUR = " + alphaColour);
        currentSize = transform.localScale.x;
        alphaColour = (maxSize - (currentSize -1f)) / maxSize;
        //Debug.Log(mat.color.a);
        Debug.Log("NOW " + alphaColour);
        mat.SetColor("_Color", new Color(mat.color.r, mat.color.g, mat.color.b, alphaColour));
    }
}
