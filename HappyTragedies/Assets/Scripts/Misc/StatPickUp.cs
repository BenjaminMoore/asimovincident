﻿using UnityEngine;
using System.Collections;

public class StatPickUp : MonoBehaviour
{

    public PlayerStats.STATS m_StatType;
    public int m_levelBoost;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>().UpgradeStat(m_StatType, m_levelBoost);
        Destroy(gameObject);
    }
}
