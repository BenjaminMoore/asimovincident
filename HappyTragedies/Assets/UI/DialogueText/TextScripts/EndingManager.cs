﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndingManager : MonoBehaviour {

	public float endingRunTime;
	private float endingStartTime;
	public float startSilence;

	public GameObject ana;

	public float[] anaDialogueStarts;
	private int anaStartCounter;
	public float[] anaDialogueEnds;
	private int anaEndCounter;

	public float[] jJDialogueStarts;
	private int jJStartCounter;
	public float[] jJDialogueEnds;
	private int jJEndCounter;

	public GameObject anaTextImport;
	public GameObject jJTextImport;

	public GameObject anaTextBox;
	public GameObject jJTextBox;

	private string[] anaLines;
	private string[] jJLines;

	public AudioSource dialogueAudio;

	public float timeToStartIntertitle;						//if no silence at start, ideal start is 1.1
	public float timeToEndIntertitle;
	public float timeToMoveAna;
	public float timeToBlack;

	public GameObject intertitleCanvas;
	public GameObject intertitle;
	public GameObject mainCam;
	public GameObject intertitleCam;
	public GameObject blackScreen;
	public GameObject cameraPivot;


	void Awake () 
	{
		ana.GetComponent<Animator> ().enabled = false;
		cameraPivot.SetActive (false);
	}

	void Start () 
	{
		endingStartTime = Time.time;
		anaStartCounter = 0;
		anaEndCounter = 0;
		jJStartCounter = 0;
		jJEndCounter = 0;

		//get imported text of dialogue
		anaLines = anaTextImport.GetComponent<ImportText>().textLines;
		jJLines = jJTextImport.GetComponent<ImportText>().textLines;

		//make sure bubbles start inactive
		anaTextBox.SetActive (false);
		jJTextBox.SetActive (false);

		if (dialogueAudio != null) {
			dialogueAudio.Play ();
		}
		intertitleCanvas.SetActive (true);
		intertitle.SetActive (false);
		intertitleCam.SetActive (true);
		mainCam.SetActive (false);
	}

	// Update is called once per frame
	void Update () 
	{
		//Find out how long intro has been running.
		endingRunTime = Time.time - endingStartTime - startSilence;
		CheckIfStartingAna ();
		CheckIfEndingAna ();
		CheckIfStartingJJ ();
		CheckIfEndingJJ ();
		CheckIfMoveAna ();
		CheckIfStartingIntertitle ();
		CheckIfEndingIntertitle ();
		CheckIfStartCameraPivot ();
	}

	public void CheckIfStartingAna()
	{
		if (anaStartCounter <= anaDialogueStarts.Length - 1)
		{
			if (endingRunTime >= anaDialogueStarts [anaStartCounter]) 
			{
				anaTextBox.SetActive (true);
				//StartDialogueScroll
				anaTextBox.GetComponent<Text>().text = anaLines[anaStartCounter];
				anaStartCounter += 1;
			}
		}
	}

	public void CheckIfEndingAna()
	{
		if (anaEndCounter <= anaDialogueEnds.Length - 1)
		{
			if (endingRunTime >= anaDialogueEnds [anaEndCounter]) 
			{
				//RemoveDialogue
				anaTextBox.SetActive (false);
				anaEndCounter += 1;
			}
		}
	}

	public void CheckIfStartingJJ()
	{
		if (jJStartCounter <= jJDialogueStarts.Length - 1) 
		{
			if (endingRunTime >= jJDialogueStarts [jJStartCounter]) 
			{
				jJTextBox.SetActive (true);
				if (jJStartCounter == 0) {
					jJTextBox.GetComponent<Text> ().fontStyle = FontStyle.BoldAndItalic;
				} else {
					jJTextBox.GetComponent<Text> ().fontStyle = FontStyle.Bold;
				}
				//StartDialogueScroll
				jJTextBox.GetComponent<Text>().text = jJLines[jJStartCounter];
				jJStartCounter += 1;
			}
		}
	}

	public void CheckIfEndingJJ()
	{
		if (jJEndCounter <= jJDialogueEnds.Length - 1) 
		{
			if (endingRunTime >= jJDialogueEnds [jJEndCounter]) 
			{
				//RemoveDialogue
				jJTextBox.SetActive (false);
				jJEndCounter += 1;
			}
		}
	}

	public void CheckIfMoveAna()
	{
		if (endingRunTime >= timeToMoveAna)
		{
			ana.GetComponent<Animator> ().enabled = true;
		}
		if (endingRunTime >= timeToBlack) 
		{
			blackScreen.SetActive (true);
		}
	}

	public void CheckIfStartingIntertitle()
	{
		if (endingRunTime >= timeToStartIntertitle && intertitle)
		{
			intertitle.SetActive (true);
		}
	}

	public void CheckIfEndingIntertitle()
	{
		if (endingRunTime >= timeToEndIntertitle && intertitle)
		{
			intertitleCanvas.SetActive (false);
			mainCam.SetActive (true);
			intertitleCam.SetActive (false);
		}
	}

	public void CheckIfStartCameraPivot()
	{
		if (endingRunTime >= 0 && cameraPivot.activeSelf == false)
		{
			cameraPivot.SetActive(true);
		}
	}
}
