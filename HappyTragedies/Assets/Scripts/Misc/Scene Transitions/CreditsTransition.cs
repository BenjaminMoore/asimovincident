﻿using UnityEngine;
using UnityEngine.SceneManagement; 
using System.Collections;

public class CreditsTransition : MonoBehaviour {

	//===========
	//Public Vars
	public float m_SceneLength;
	public int m_LoadSceneNumber;
    public bool m_CreditsScene;
	//===========

	//===============
	//privare vars
	public float m_DurationCounter; 
	//==============
	// Use this for initialization
	void Start () {
		m_DurationCounter = 0.0f;  
	}

	// Update is called once per frame
	void Update () {
		m_DurationCounter += Time.deltaTime; 

		if (m_DurationCounter >= m_SceneLength || (Input.GetButtonDown("Fire1") && !m_CreditsScene))
		{
            if(m_CreditsScene)
            {
                GameManager.instance.gameObject.SetActive(true);
                GameManager.instance.LoadMenu();
            }
            else
			SceneManager.LoadScene(m_LoadSceneNumber); 
		}
	}
}
