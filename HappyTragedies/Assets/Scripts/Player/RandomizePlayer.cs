﻿using UnityEngine;
using System.Collections;

public class RandomizePlayer : MonoBehaviour
{

    public Texture[] hairTextures;
    public Texture[] hairTextureLong;
    public Texture[] hairAfro;
    public Texture[] hairBob;
    GameObject longHairMesh;
    GameObject shortHairMesh;
    GameObject bobHairMesh;
    GameObject afroHairMesh;
    private Material hairMat;
    bool longHair;
    // Use this for initialization
    void Awake()
    {
        //get the material and randomise it with random hair texture

        Transform[] childrenObj = GetComponentsInChildren<Transform>();
        foreach (Transform t in childrenObj)
        {
            if (t.tag == "Long Hair")
            {
                longHairMesh = t.gameObject;
            }
            if (t.tag == "Short Hair")
            {
                shortHairMesh = t.gameObject;
            }
            if (t.tag == "HairAfro")
            {
                afroHairMesh = t.gameObject;
            }
            if (t.tag == "HairBob")
            {
                bobHairMesh = t.gameObject;
            }
        }

        longHairMesh.SetActive(false);
        shortHairMesh.SetActive(false); ;
        bobHairMesh.SetActive(false); ;
        afroHairMesh.SetActive(false); ;
        int ChooseHairType = Random.Range(0, 4);
        switch (ChooseHairType)
        {
            case 0:
                {
                    longHairMesh.SetActive(true);
                    hairMat = longHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairTextureLong[Random.Range(0, hairTextures.Length)];
                    break;
                }
            case 1:
                {
                    shortHairMesh.SetActive(true);
                    hairMat = shortHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairTextures[Random.Range(0, hairTextures.Length)];
                    break;
                }
            case 2:
                {
                    bobHairMesh.SetActive(true);
                    hairMat = bobHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairBob[Random.Range(0, hairTextures.Length)];
                    break;
                }
            case 3:
                {
                    afroHairMesh.SetActive(true);
                    hairMat = afroHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairAfro[Random.Range(0, hairTextures.Length)];
                    break;
                }
            default: break;
        }

    }

    public void RandomiseHair()
    {
        longHairMesh.SetActive(false);
        shortHairMesh.SetActive(false); ;
        bobHairMesh.SetActive(false); ;
        afroHairMesh.SetActive(false); ;
        int ChooseHairType = Random.Range(0, 4);
        switch (ChooseHairType)
        {
            case 0:
                {
                    longHairMesh.SetActive(true);
                    hairMat = longHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairTextureLong[Random.Range(0, hairTextures.Length)];
                    break;
                }
            case 1:
                {
                    shortHairMesh.SetActive(true);
                    hairMat = shortHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairTextures[Random.Range(0, hairTextures.Length)];
                    break;
                }
            case 2:
                {
                    bobHairMesh.SetActive(true);
                    hairMat = bobHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairBob[Random.Range(0, hairTextures.Length)];
                    break;
                }
            case 3:
                {
                    afroHairMesh.SetActive(true);
                    hairMat = afroHairMesh.GetComponentInChildren<Renderer>().material;
                    hairMat.mainTexture = hairAfro[Random.Range(0, hairTextures.Length)];
                    break;
                }
            default: break;
        }
    }
}
