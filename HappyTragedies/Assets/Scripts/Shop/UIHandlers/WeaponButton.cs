﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponButton : MonoBehaviour {

    //public vars
    [Tooltip("Where the button is in the UI heirachy, 0 indexed (Begins at 0)")]
    public int buttonNumber;

    //private vars
    private ShopFunctions shopfunctions;
	void Start () {
        shopfunctions = GetComponentInParent<ShopFunctions>();
	}
	
    public void ButtonClicked()
    {
        shopfunctions.PurchaseWeapon(buttonNumber);
        Destroy(this);
    }
}
