﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Author Benjamin Moore
 * This Script Contains Functions for the ShopUI to call to perform functions 
 */
public class ShopFunctions : MonoBehaviour {


    //Private Vars
    //the storeGenerator code has the created weapons and associated costs
    GenerateStoreContents theStoresGenerator;
    //Acess to buttons used to burchase weapons
    ManageShop shopManager;
    //keeps track of which weapons have been purchased already
    bool[] weapPurchased;
    //determines players current weapon type
    bool gunActive;
    PlayerController player;
    // Use this for initialization
    ShopNameHandler[] nameManager;
    ShopCostHandler[] costManager;
    void Awake()
    {
        //get components in awake to ensure they arent disabled
        theStoresGenerator = GetComponentInParent<GenerateStoreContents>();
        weapPurchased = new bool[3];
        //initally no weps have been purchased
        for (int i = 0; i < 3; i++)
        {
            weapPurchased[i] = false;
        }
        nameManager = new ShopNameHandler[3];
        nameManager = GetComponentsInChildren<ShopNameHandler>();
        costManager = new ShopCostHandler[3];
        costManager = GetComponentsInChildren<ShopCostHandler>();
    }

    void Start()
    {
        //Get object references
        player = FindObjectOfType<PlayerController>();

    }

    //sorts names into order
    void sortNamesIntoOrder()
    {
        ShopNameHandler[] tempHolder;
        tempHolder = new ShopNameHandler[3];
        for (int i =0; i < 3; i++)
        {
            int WepPos = nameManager[i].nameNumber;
            switch (WepPos)
                {
                case 0:
                    tempHolder[0] = nameManager[i];
                    break;
                case 1:
                    tempHolder[1] = nameManager[i];
                    break;
                case 2:
                    tempHolder[2] = nameManager[i];
                    break;
                default:
                    Debug.Log("Unexpected Switch Entry, Name Ordering");
                    break;
                }
        }
        nameManager = tempHolder;
    }


    //Purchase weapon
    public void PurchaseWeapon(int ButtonUsed)
    {
        //deactivate the players weapon while purchasing
        checkActiveWeaponType();
        if (gunActive)
        {
            player.gameObject.GetComponentInChildren<GunAttack>().enabled = false;
        }
        else
            player.gameObject.GetComponentInChildren<MeleeAttack>().enabled = false;


        if (weapPurchased[ButtonUsed] == true)
        {
            return;
        }
        //check if they have enough money to purchase weapon
        float money = player.GetBank();
        int wepCost = theStoresGenerator.GetWeapCost(ButtonUsed);
        if (money < wepCost)
        {
            return;
        }

        //assign them the chosen weapon
        switch (ButtonUsed)
            {
            case 0:
                theStoresGenerator.GetWeapon(0).SetActive(true);
                player.TakeBank(wepCost);
                player.SetWeapon(theStoresGenerator.GetWeapon(0));
                nameManager[0].wepPurchased();
                weapPurchased[0] = true;
                break;
            case 1:
                theStoresGenerator.GetWeapon(1).SetActive(true);
                player.TakeBank(wepCost);
                player.SetWeapon(theStoresGenerator.GetWeapon(1));
                nameManager[1].wepPurchased();
                weapPurchased[1] = true;
                break;
            case 2:
                theStoresGenerator.GetWeapon(2).SetActive(true);
                player.SetWeapon(theStoresGenerator.GetWeapon(2));
                player.TakeBank(wepCost);
                nameManager[2].wepPurchased();
                weapPurchased[2] = true;
                break;
            default:
                Debug.Log("Unexpected Switch Result");
                break;
        }
    }
	
    void closeShop()
    {
        shopManager.CloseShop();
    }

    //determines the weapon which the player holds
    void checkActiveWeaponType()
    {
        if (player.gameObject.GetComponentInChildren<MeleeAttack>() != null)
            gunActive = false;
        else if (player.gameObject.GetComponentInChildren<GunAttack>() != null)
            gunActive = true;
        else
            Debug.Log("No weapon Attack Script");
    }
    public void setCostUI()
    {
        for (int i = 0; i < 3; i++)
        {
            costManager[i].GenerateStoreText();
        }
    }

    public void SetNameUI()
    {
        for (int i = 0; i < 3; i++)
        {
            nameManager[i].GenerateName();
        }
    }
}
