﻿using UnityEngine;
using UnityEngine.SceneManagement; 
using System.Collections;

public class EndingTransitions : MonoBehaviour {

    //================
    //Public Vars 
    public int m_FinalCutsceneSceneNumber; 
    //================

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //loads the scene to play the closing cutscene
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            PlayerController pc = col.GetComponent<PlayerController>();
            pc.SetAnimationState(0);
            pc.TurnOffInvincible();
            pc.enabled = false;
            pc.transform.LookAt(new Vector3(0, pc.transform.position.y, 10));
            pc.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
            GameObject.FindGameObjectWithTag("GameManager").SetActive(false);
            GameObject.FindGameObjectWithTag("SoundManager").SetActive(false);
            GameObject.FindGameObjectWithTag("Stats").SetActive(false);
            SceneManager.LoadScene(m_FinalCutsceneSceneNumber);
        }            
    }
}
