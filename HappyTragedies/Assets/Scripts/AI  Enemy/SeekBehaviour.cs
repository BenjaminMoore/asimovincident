﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS 
/// Last Editted: 9/7/2017 by Douglas Landers 
/// The seek class is a simple AI behaviour that sets the destination of the 
/// </summary>
public class SeekBehaviour : AIBehaviour
{

    //===============
    //Private Vars 
    GameObject m_Player;                    //A reference to the players gameobject
    UnityEngine.AI.NavMeshAgent m_Agent;    //A reference to the AI's navmeshagent
    EnemyAI m_AI;                           //A reference to the AI's EnemyAI component
    //===============

    public override eResult Initialise()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (m_AI && m_Agent && m_Player)
            return eResult.Success;
        else return eResult.Fail; 
    }

    //All we need to do is set the AI's navMeshAgent to the player's position anf return success
    public override eResult Execute()
    { 
        if (m_Player && m_Agent && m_AI)                            //if we have valid references execute our stuff
        {
            m_Agent.SetDestination(m_Player.transform.position);    //Set the Agents navmesh destination to the player position
            m_AI.SetActionState(1);                                 //Update the AIs action state
            m_AI.SetAnimationState(1);                              //used to set the animation for the AI to walking
            return eResult.Success;
        }
        else
            return eResult.Fail; 
    }

    //Checks to see if the behaviour can be executed
    public override eResult Evaluate()
    {
        //only return success if the AI caqn see the player 
        if (m_AI.CanSeePlayer())
            return eResult.Success;
        else return eResult.Fail;
    }

}
