﻿using UnityEngine;
using System.Collections;

public class StunParticles : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnParticleCollision(GameObject other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<PlayerController>().RecieveStun(GetComponentInParent<BossAI>().m_StunDuration);
            gameObject.GetComponent<ParticleSystem>().Stop();
        } 
    }
}
