﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WTSLockUI : MonoBehaviour
{

    public Text[] m_statFields;
        
    public GameObject active;
    public GameObject inActive;

    private Color m_baseColor;
    
    void Awake()
    {
        m_baseColor = m_statFields[0].color;
    }
    // Use this for initialization
    void Start()
    {
        active.SetActive(true);
        inActive.SetActive(false);
    }

    public void UnlockSystem()
    {
        active.SetActive(true);
        inActive.SetActive(false);
    }

    public void LockSystem()
    {
        active.SetActive(false);
        inActive.SetActive(true);
    }

    public void SetStats(Weapon a_playerWep, Weapon a_stashWep)
    {
        m_statFields[0].text = a_playerWep.m_damage.ToString("F1");
        m_statFields[1].text = a_stashWep.m_damage.ToString("F1");
        m_statFields[2].text = a_playerWep.m_attackRate.ToString("F1");
        m_statFields[3].text = a_stashWep.m_attackRate.ToString("F1");
        m_statFields[4].text = a_playerWep.m_reloadTime.ToString("F1");
        m_statFields[5].text = a_stashWep.m_reloadTime.ToString("F1");

        m_statFields[0].color = a_playerWep.m_damage > a_stashWep.m_damage ? Color.green : Color.red;
        m_statFields[1].color = a_stashWep.m_damage > a_playerWep.m_damage ? Color.green : Color.red;
        if(a_playerWep.m_damage == a_stashWep.m_damage)
        {
            m_statFields[0].color = m_baseColor;
            m_statFields[1].color = m_baseColor;
        }

        m_statFields[2].color = a_playerWep.m_attackRate > a_stashWep.m_attackRate ? Color.green : Color.red;
        m_statFields[3].color = a_stashWep.m_attackRate > a_playerWep.m_attackRate ? Color.green : Color.red;
        if (a_playerWep.m_attackRate == a_stashWep.m_attackRate)
        {
            m_statFields[2].color = m_baseColor;
            m_statFields[3].color = m_baseColor;
        }

        m_statFields[4].color = a_playerWep.m_reloadTime < a_stashWep.m_reloadTime ? Color.green : Color.red;
        m_statFields[5].color = a_stashWep.m_reloadTime < a_playerWep.m_reloadTime ? Color.green : Color.red;
        if (a_playerWep.m_reloadTime == a_stashWep.m_reloadTime)
        {
            m_statFields[4].color = m_baseColor;
            m_statFields[5].color = m_baseColor;
        }
    }

}
