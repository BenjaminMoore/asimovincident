﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerNew : MonoBehaviour {

	public AudioSource[] effectSources;
    public AudioSource FootStepsSource;
	public AudioSource backgroundMusicSource;
	public static SoundManagerNew instance = null;

    [Header("Environmental Sounds")]
    public AudioClip DoorsOpenSFX;
    public AudioClip DoorsCloseSFX;
    public AudioClip DoorsLockSFX;

    [Header("Menu Sounds")]
    public AudioClip menuMoveSFX;
    public AudioClip menuSelectSFX;
    public AudioClip deadBackgroundSFX;

    [Header("BackgroundMusix")]
    public AudioClip MainMenuBackgroundMusic;
    public AudioClip MainSceneMusic;
    public AudioClip AlternateSceneMusic;
    public AudioClip BossSceneMusic;

    [Header("FootSteps")]
    public AudioClip footStep1; 
    public AudioClip footStep2;
    public AudioClip footStep3;
    public AudioClip footStep4;

    // Set up the sound manager as a singleton and dont destroy between scenes
    void Awake()
	{

		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null)
			Destroy(gameObject);
        SetBackgroundMusic(MainMenuBackgroundMusic);
        DontDestroyOnLoad(gameObject);
	}

    //this public function will play any audio clip passed into it
    //Possbile issue may occur when multiple SFX try to play at once
	public void PlaySFX(AudioClip clip)
	{
        for (int i = 0; i < effectSources.Length; i++)
        {
            if (!effectSources[i].isPlaying)
            {
                effectSources[i].clip = clip;
                effectSources[i].Play();
                return;
            }
        }
	}

    //Special functions to play menu sounds so the sfx can be on the sound manager
    public void MenuMove()
    {
        PlaySFX(menuMoveSFX);
    }
    
    public void MenuSelect()
    {
        PlaySFX(menuSelectSFX);
    }

    //sets the background music by taking in an audio clip
    public void SetBackgroundMusic(AudioClip backgroundMusic)
    {
        backgroundMusicSource.clip = backgroundMusic;
        backgroundMusicSource.Play();
    }

    //Toggles the mute of SFX
    public void ToggleSFX()
    {
        for(int i =0; i< effectSources.Length; i++)
        effectSources[i].mute ^= true;
    }

    //Toggles the mute of background music
    public void ToggleBackgroundMusic()
    {
        backgroundMusicSource.mute ^= true;
    }


    public void PlayFootSteps(AudioClip clip)
    {
            if (!FootStepsSource.isPlaying)
            {
                FootStepsSource.clip = clip;
                FootStepsSource.Play();
                return;
            }
    }
}
