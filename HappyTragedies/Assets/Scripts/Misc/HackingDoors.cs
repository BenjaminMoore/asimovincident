﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HackingDoors : MonoBehaviour {
    public float timeToHack;
    bool hackFinished;
    bool inProximity;
    public float TimeHacking;
    GameObject HackUI;
    Image HackBar;
	// Use this for initialization
	void Start () {
        hackFinished = false;
        inProximity = false;
        HackUI = GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIToggle>().HackUI;
        HackBar = HackUI.GetComponentInChildren<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (inProximity)
        {
            if(tag == "Door")
            {
                return;
            }
            if(Input.GetKey(KeyCode.Q))
            {
                HackUI.SetActive(true);
                TimeHacking += Time.deltaTime;
                HackBar.fillAmount = TimeHacking / timeToHack;
                Debug.Log("HACKING " + TimeHacking);
            }
        }
	    if(TimeHacking > timeToHack)
        {
            hackFinished = true;
            tag = "Door";
            GetComponent<Door>().OpenDoor();
            Debug.Log("Door opened");
            TimeHacking = 0;
            HackUI.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            HackBar.fillAmount = 0;
            HackUI.SetActive(false);
            TimeHacking = 0;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (tag == "Locked")
        {
            inProximity = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            inProximity = false;
            TimeHacking = 0;
        }
    }
}
