﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS
/// Last Editted: 9/7/2016 by Douglas Landers
/// The AIBehaviour script is the parent script for all AI behaviours.
/// </summary>
public class AIBehaviour : MonoBehaviour
{
    //Enum states are used to inform the AI classes whether or not a behaviour can be started, and keeps track of the progress of a behaviour
    public enum eResult 
    {
        Success     = 0, 
        Fail        = 1, 
        InProgress  = 2
    }

    //Used for gathering references required to execute the behaviour. 
    public virtual eResult Initialise()
    {
        return eResult.Fail; 
    }

    //Execute function is where the functional logic of a behaviour is executed. for example, playing animations. 
    public virtual eResult Execute()
    {
        return eResult.Fail; 
    }

    //Evalute function is where the pre-emptive logic of a behavior is calculate, for example, determining if it can be executed, and obtaining needed references. 
    public virtual eResult Evaluate()
    {
        return eResult.Fail; 
    } 

}
