﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS  
/// DATE: 17th July 2016 
/// CURRENT STATE: Basic Setup Completed :: Development not begun. 
/// PURPOSE: 
/// WeaponEffects are a pre-defined effect that is placed on a weapon to increase damage or firing speed, etc.  
///
/// TODO :: 
///  -  Everything
/// </summary>
public class WeaponEffect : MonoBehaviour {

    //==========================
    //Public Vars 
    [Tooltip ("The name of the enhancent")]
    string m_name;        
    [Tooltip("The factor this enhancement increase damage by")]
    float m_fDamageModifier;   
    [Tooltip("the factor this enhancement increases mag capacity by")]
    float m_fMagazineSizeModifier;  
    [Tooltip("the factor this enhancement increases firing speed by")]
    float m_fRateOfFireModifier;  
    //==========================

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
