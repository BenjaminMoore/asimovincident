﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopCostHandler : MonoBehaviour {

    // Use this for initialization
    [Tooltip("Where the name is in the UI heirachy, 0 indexed (Begins at 0)")]
    public int nameNumber;


    //private vars
    private ShopFunctions shopfunctions;
    GenerateStoreContents store;
    Text gameText;
    private void Awake()
    {
        store = GetComponentInParent<GenerateStoreContents>();
        shopfunctions = GetComponentInParent<ShopFunctions>();
        gameText = GetComponent<Text>();
    }

    public void GenerateStoreText()
    {
        int wepCost = store.GetWeapCost(nameNumber);
        gameText.text = wepCost.ToString();
    }
}
