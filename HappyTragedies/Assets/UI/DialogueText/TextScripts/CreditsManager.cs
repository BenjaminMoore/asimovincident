﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreditsManager : MonoBehaviour {

	public float creditsRunTime;
	private float creditsStartTime;

	public float[] nextCredits;
	public int[] whichCredits;
	private int creditsCounter;

	public AudioSource creditsAudio;

	public GameObject asimovIncident;
	public GameObject projectLead;
	public GameObject castVO;
	public GameObject designer;
	public GameObject programmer;
	public GameObject artist;
	public GameObject happyTragedies;

	// Use this for initialization
	void Start () 
	{
		creditsStartTime = Time.time;
		creditsCounter = 0;

		if (creditsAudio != null) {
			creditsAudio.Play ();
			Debug.Log ("play");
		}
	}

	// Update is called once per frame
	void Update () 
	{
		//Find out how long credits has been running.
		creditsRunTime = Time.time - creditsStartTime;

		if (creditsCounter <= whichCredits.Length - 1 && creditsCounter <= nextCredits.Length - 1)
		{
			CheckIfNextCredits (whichCredits [creditsCounter]);
		}
	}

	public void CheckIfNextCredits(int xxx)
	{
		if (creditsRunTime >= nextCredits [creditsCounter]) 
		{
			//none = 0
			//AsimovINcident = 1
			//Cast = 2
			//ProjectLead = 3
			//Designers = 4
			//Programmers = 5
			//Artists = 6
			//HappyTragedies = 7

			asimovIncident.SetActive (false);
			projectLead.SetActive (false);
			castVO.SetActive (false);
			designer.SetActive (false);
			programmer.SetActive (false);
			artist.SetActive (false);
			happyTragedies.SetActive (false);

			if (xxx == 0) {
				//none
			} else if (xxx == 2) {
				projectLead.SetActive (true);
			} else if (xxx == 3) {
				castVO.SetActive (true);
			} else if (xxx == 4) {
				designer.SetActive (true);
			} else if (xxx == 5) {
				programmer.SetActive (true);
			} else if (xxx == 6) {
				artist.SetActive (true);
			} else if (xxx == 7) {
				happyTragedies.SetActive (true);
			} else {
				asimovIncident.SetActive (true);
			}

			creditsCounter += 1;
		}
	}
}
