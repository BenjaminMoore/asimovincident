﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS  
/// DATE: 17th July 2016 
/// CURRENT STATE: Work In Progress 
/// PURPOSE: 
/// This is the basic class for a gun style weapon, it uses a hit scan algorithm to detect if it has hit something. 
/// It uses game object tags to determine if it has hit a target it can do damage to, then obtains that targets health script to do damage to it. 
/// </summary>


public class GunAttack : MonoBehaviour {

    //===================================================
    //Public Vars 
    [Tooltip("How many times the gun fires a second")]
    public float m_RateOfFire;
    [Tooltip("How many bullets can be fired before reloading")]
    public int m_MagzineSize;
    [Tooltip("What object tag will result in a 'hit'")]
    public string m_TargetTag;
    [Tooltip("The Maximum number of weapon effects a weapon can have")]
    public int m_MaxWeaponEffects = 3;
    [Tooltip("Weapon Effects can change the properties of different weapons")]
    public WeaponEffect[] m_WeaponEffects;
    [Tooltip("Bullet particle system prefab")]
    public GameObject m_bulletParticle;
    [Tooltip("The amount of time the muzzle flash will be played for (if using the billboard)")]
    public float m_MuzzleFlashDuration;
    [Tooltip("Shake duration for Shotgun only")]
    public float m_ShotgunShakeDur;
    [Tooltip("Shake intensity for Shotgun only")]
    public float m_ShotgunShakeIntensity;

    [Header("Sound Files")]
    public AudioClip noAmmoSFX;         //Sound Queue for when the clip is empty but the player tries to fire. 
    public AudioClip gunFire;           //The Sound Queue that the gun has been fired. 
    public AudioClip reloadStart;       //The first sound queue that the reload has been started
    public AudioClip reloadFinish;      //The sound queue that the reload has been finished. 
    //===================================================

    //read me please
    // [ok, I'm reading you (: ]
    //alright you can stop reading my now before i start blushing

    //===================================================
    //Private Vars

    double m_ReloadProgression;     //How long since reload started.
    double m_ReloadTime;            //How long it takes to reload the weapon. 
    double m_SinceLastShot;         //How long since the last bullet was fired. 
    int m_BulletsRemaining;         //How many bullets remain in the magazine
    ParticleSystem m_MuzzleFlash;   //A reference to a empty child object where the muzzleflash should be played. 
    GameObject m_MuzzleFlashQuad;   //A reference to the gameobject that contains the quads with the muzszle flash texture
    PlayerController m_Player;      //A reference to the player controller
    Weapon m_weaponScript;          //The weapon script that is attached to this game object. 
    float m_Damage;                 //The amount of damage that is dealt when an enemy is hit. 
    bool m_ReloadStarted = false;   //Whether or not the reload has been started. 
    bool m_FireButtonDown = false;  //Whether or not the "fire" button is pressed down. 

    //Variables for Deletion (Maybe, need to check first) 
    public GameObject m_Sparks;
    private Image[] m_reloadUI;
    //===================================================

    // Use this for initialization
    void Start()
    {
        //Initialise Variables 
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        m_weaponScript = GetComponent<Weapon>();
        m_Damage = m_weaponScript.m_damage;
        m_SinceLastShot = 5;
        m_MagzineSize = m_weaponScript.m_clipSize;
        m_BulletsRemaining = m_MagzineSize;
        m_RateOfFire = m_weaponScript.m_attackRate;
        m_ReloadTime = m_weaponScript.m_reloadTime;
        m_WeaponEffects = new WeaponEffect[m_MaxWeaponEffects];

        //Use this game objects tag to determine m_TargetTag ***********************Is target tag still used?? 
        if (gameObject.tag == "Player")
            m_TargetTag = "Enemy";
        else if (gameObject.tag == "Enemy")
            m_TargetTag = "Player";

        //Get a reference to our muzzleflash 
        if (m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_RIFLE || m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
        {
            m_MuzzleFlashQuad = transform.GetChild(0).gameObject;
            m_MuzzleFlashQuad.SetActive(false);                             //turn off the GO by default
        }
        else
            m_MuzzleFlash = GetComponentInChildren<ParticleSystem>();

        //Initialise the UI for AMMO ***This May be Moved into the UI Script to make UI systems more centralised*** 
        GameObject[] Clips = GameObject.FindGameObjectsWithTag("GunClip");  //Retrieves the Ammo UI Foreground and the "RELOADING" UI image
        m_reloadUI = new Image[2];
        foreach (GameObject Clip in Clips)
        {
            if (Clip.GetComponent<Image>().type == Image.Type.Filled)       //This will sort the Ammo Counting UI into the first element and fill it 100% 
            {
                m_reloadUI[0] = Clip.GetComponent<Image>();
                m_reloadUI[0].fillAmount = ((float)m_BulletsRemaining / (float)m_MagzineSize);
            }
            else if (Clip.GetComponent<Image>().type == Image.Type.Simple)  //This will sort the "RELOADING" Image and turn it off by default. 
            {
                m_reloadUI[1] = Clip.GetComponent<Image>();
                m_reloadUI[1].enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        m_SinceLastShot += Time.deltaTime;                  //Update the time its been since the gun fired. 

        //Do Pre-Update checks
        if (Input.GetAxisRaw("Fire1") == 0)                 //Check to see if the Fire-Axis has been released since it was last pressed. (necessary for the Input.GetButtonDown substitute used for Gamepad compatability) 
            m_FireButtonDown = false;                       //Reset the Button-Down substitute
        if (m_MuzzleFlash && m_MuzzleFlash.isPlaying)       //If the MuzzleFlash is still playing disable it
            m_MuzzleFlash.Stop();
        if (m_Player != null && m_Player.GetIsStunned())    //If the player is stunned update is unnessesary
            return;
        //End Pre-Update checks

        if (m_BulletsRemaining <= 0)                        //Check to see if the weapon needs to be reloaded
        {
            if (Input.GetAxisRaw("Fire1") != 0)             //If the player is trying to dry-fire, play the empty-clip sound
                PlaySFX(noAmmoSFX);
            Reload();                                       //Start Reloading 
        }
        else if (Input.GetButtonDown("Reload") && m_BulletsRemaining != m_MagzineSize) //Check if the player wants to reload
            Reload();                                       //Start Reloading

        if (Input.GetAxisRaw("Fire1") != 0 && !m_ReloadStarted)                  //If the player is attempting to fire
        {
            if (m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
                FireSingle();
            else if (m_SinceLastShot >= 1 / m_RateOfFire)
                FireAuto();
        }

        if (m_reloadUI[0] != null)                          //Update The AMMO UI **may be moved into seperate UI script**
            m_reloadUI[0].fillAmount = ((float)m_BulletsRemaining / (float)m_MagzineSize);

        if (Input.GetAxisRaw("Fire1") == 0 || m_BulletsRemaining <= 0) //Check to see if the player's walkspeed needs to be re-adjusted
            GetComponentInParent<PlayerController>().SlowWalkSpeed(false);
    }


    //FireSingle is called to fire a single shot per button press, this is done using the GetAxisRaw (As a gamepad may be used) and using m_FireButtonDown as a substitute for Input.GetButtonDown
    void FireSingle()
    {
        if (!m_FireButtonDown)
        {
            m_FireButtonDown = true;
            GetComponentInParent<PlayerController>().PlaySFX(gunFire);  //Play Gunshot
            --m_BulletsRemaining;                                       //Lower Remaining bullets
            Instantiate(m_bulletParticle, transform.GetChild(0).position, transform.rotation); //Spawn the BulletParticle, and send it forward from the guns orientation
            StartCoroutine(PlayMuzzleFlash(m_MuzzleFlashDuration));     //Play muzzle flash
            m_SinceLastShot = 0f;
        }
    }

    //FireAuto is called when firing an automatic weapon, will fire for as long as the mouse is held down and there is ammo in the clip, provided there has been a long enough pause between shots (tracked with m_RateOfFire and m_SinceLastShot) 
    void FireAuto()
    {
        switch (m_weaponScript.m_WeaponType)
        {
            case WEAPON_TYPE.WEAPON_RIFLE:
                PlaySFX(gunFire);
                GetComponentInParent<PlayerController>().SlowWalkSpeed(true);                                   //MovementSpeed on the PlayerController is slowered while continuously firing rifles. 
                break;
            case WEAPON_TYPE.WEAPON_SHOTGUN:                                                                    //ScreenShake is applied when firing shotguns. 
                if (!Camera.main.GetComponent<FollowPlayer>().Shaking())
                    Camera.main.GetComponent<FollowPlayer>().Shake(m_ShotgunShakeDur, m_ShotgunShakeIntensity);
                PlaySFX(gunFire);
                break;
            default:
                break;
        }
        //Spawn the Projectile and Play the muzzleflash, Determine which weapon the player is using to decide which method to use
        if (m_weaponScript.m_WeaponType != WEAPON_TYPE.WEAPON_RIFLE)   //RIFLE METHOD
        {
            m_MuzzleFlash.Play();                                                                   //Play Muzzleflash
            Instantiate(m_bulletParticle, m_MuzzleFlash.transform.position, transform.rotation);    //Spawn Bullet
        }
        else                                                           //OTHER METHOD 
        {
            StartCoroutine(PlayMuzzleFlash(m_MuzzleFlashDuration));                                  //Play muzzle flash
            Instantiate(m_bulletParticle, m_MuzzleFlashQuad.transform.position, transform.rotation); //Spawn Bullet
        }

        --m_BulletsRemaining; //Remove bullet from clip
        m_SinceLastShot = 0;  //Reset the Timer
    }

    public void UpdateWeapon()
    {
        m_Damage = GetComponent<Weapon>().m_damage;
        m_RateOfFire = GetComponent<Weapon>().m_attackRate;
        m_ReloadTime = GetComponent<Weapon>().m_reloadTime;
        UpdateWeaponStats();
    }

    public void UpdateWeaponStats() //*******MARK FOR DELETE**************// (Refactor of WEAPON script required before it can be given the clear) 
    {
        //m_weaponScript.m_clipSize = m_MagzineSize;
        //m_weaponScript.m_currentClip = m_BulletsRemaining;
        //m_weaponScript.UpdateUIStats();
    }

    //Plays a muzzle flash by enable and disable a Quad with the muzzleflash texture on it
    IEnumerator PlayMuzzleFlash(float a_duration)
    {
        m_MuzzleFlashQuad.SetActive(true);
        yield return new WaitForSeconds(a_duration);
        m_MuzzleFlashQuad.SetActive(false);
    }

    //Plays animations and SFX for reloading, as well as all associated reload logic
    void Reload()
    {
        if(!m_ReloadStarted)
        {
            m_ReloadStarted = true;                                 //Flag the reload as started
            m_ReloadProgression = 0.0;                              //reset the ReloadProgression tracker
            m_BulletsRemaining = 0;                                 //Empty the clip so it cannot be fired while reloading
            PlaySFX(reloadStart);                                   //Play the ReloadStart soundfile
            Animator animator = GetComponentInParent<Animator>();   //Retrieve the Players Animator
            animator.SetBool("Reloading", true);                    //Tell the Animator that Reloading has begun
            animator.SetBool("UsingShotgunReload", false);          //Default all Reloading bools
            animator.SetBool("UsingRifleReload", false);
            animator.SetBool("UsingPistolReload", false);

            switch(m_weaponScript.m_WeaponType)                     //Determine the type of weapon this is, and then set the appropriate reloading bool
            {
                case WEAPON_TYPE.WEAPON_PISTOL:
                    animator.SetBool("UsingPistolReload", true);
                    break;
                case WEAPON_TYPE.WEAPON_RIFLE:
                    animator.SetBool("UsingRifleReload", true); 
                    break;
                case WEAPON_TYPE.WEAPON_SHOTGUN:
                    animator.SetBool("UsingShotgunReload", true); 
                    break;
                default:
                    break;
            }

        }
        else if (m_ReloadProgression >= m_ReloadTime)    //If the reload has finished
        {
            m_BulletsRemaining = m_MagzineSize;     //fill the clip to maximum
            if (m_reloadUI[1] != null)
                m_reloadUI[1].enabled = false;      //Turn off the RELOADING UI image
            if (m_reloadUI[0] != null)
                m_reloadUI[0].fillAmount = 1;       //Fill the Ammo Bar to full
            PlaySFX(reloadFinish);                  //Play the reload compleat sound file
            m_ReloadStarted = false;                //Reset the reload flag
            return;                                 //Exit the function
        }
        if (m_reloadUI[1] != null)                  //Turn on the RELOADING UI image
            m_reloadUI[1].enabled = true;
        if (m_reloadUI[0]!= null)                   //Fill up the ammo bar as the reload continues
            m_reloadUI[0].fillAmount = (float)(m_ReloadProgression / m_ReloadTime);
        m_ReloadProgression += Time.deltaTime;      //increase the progression tracker
    }

    //Tells the sound manager to play the passed in audioclip 
    void PlaySFX(AudioClip a_Audio)
    {
        SoundManagerNew.instance.PlaySFX(a_Audio);
    }
}
