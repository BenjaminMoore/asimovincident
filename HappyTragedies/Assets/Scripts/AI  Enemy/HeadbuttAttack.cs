﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: Douglas Landers 
/// Last Editted: 9/7/2017
/// This behaviour is a melee attack used by the grunt typed enemies. It waits for the point of contact in the animation to deal damage 
/// to the player if they are close enough to be hit. 
/// </summary>
public class HeadbuttAttack : AIBehaviour
{
    //==================
    //Private Vars 
    eResult                     m_State = eResult.Fail; //The current stage of the behaviour | Fail = not started | In Progress = In Progress | Success = Finished 
    EnemyAI                     m_AI;                   //Reference to the EnemyAI object that owns this behaviour 
    UnityEngine.AI.NavMeshAgent m_Agent;                //Reference to the NavMeshAgent responsible for steering this object
    Rigidbody                   m_rigidBody;            //Reference to the Rigidbody attached to this object. 
    Health                      m_PlayerHealth;         //Reference to the health component of the player, used for dealing damage 
    //==================

        //Initialise the variables and references 
    public override eResult Initialise()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>(); //Get the AI component
        if (m_Agent == null)
            m_Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (m_rigidBody == null)
            m_rigidBody = GetComponent<Rigidbody>();
        if (m_PlayerHealth == null)
            m_PlayerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>(); 

        if (m_AI && m_Agent && m_rigidBody && m_PlayerHealth)
            return eResult.Success;
        else return eResult.Fail; 
    }

    //This function is used to initiate and update the behaviour. 
    public override eResult Execute()
    {
        if (m_State == eResult.Fail)        //if the behaviour has not yet been started 
        {
            m_AI.SetAnimationState(2);      //Sets the animation state to play the attack animation (state 2 on the animatior 
            m_AI.ResetAttackCounter();      //Tell the AI controller to reset the attack counter (cooldown) 
            m_Agent.enabled = false;        //Disable the NavMeshAgent so it cannot move while attacking
            m_rigidBody.constraints = RigidbodyConstraints.FreezeRotation; //Tell the rigidbody to freeze its rotation while it attacks
            m_State = eResult.InProgress;   //Update the state of the behaviour 
        }
        return m_State; 
    }

    //Checks to see if the conditions are met for this behaviours action
    public override eResult Evaluate()
    {
        m_State = eResult.Fail;         //Set the State to its default

        if (m_AI.IsPlayerClose())       //Check if the player is close enough to attack
            return eResult.Success;
        else
            return eResult.Fail; 
    }

    //Called by the animator when it is ready to inflict damage
    public void InflictDamage()
    {
        if (m_AI.IsPlayerClose()) //If the player is still close
            m_PlayerHealth.DoDamage(m_AI.m_BaseDamage); 
    }

    //Called by the animator when it finishes the attack animation
    public void FinishHeadbuttAttack()
    {
        m_AI.SetAnimationState(0);  //Set the ai back to idling animation.
        m_AI.SetActionState(1);     //Update the state of this enemy
        m_Agent.enabled = true;     //turn the nav mesh agent back on 
        m_rigidBody.constraints = RigidbodyConstraints.None; //Unfreeze the rotiation of the rigidbody
        m_State = eResult.Success;  //Update the state to show the behaviour is completed. 
         
    }

}
