﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    /*
    Owner Benjamin Moore
    Player Controller Class. Responsible for all movement of the player as well as receiving the input to fire weapons
    Last Updated 2/8/2017       
    */


	//===============================
	//Public Vars
    public int m_inputDevice = 0; // 1 = Keyboard Input detected, 2 = Controller Input detected.
	[Tooltip("The speed at which the player navigates the world")]
	public float m_MoveSpeed = 5;
	[Tooltip("A reference to the main camera used for veiwing ")]
	public Camera m_MainCamera;
	[Tooltip("The amount of time after being attacked the player is untouchable for")]
	public float m_UntouchableTime;
	[Tooltip("How much the player slows down while firing weapons")]
	public float m_speedModifierDenominator;
	[Tooltip("A handle to the Players invincinbility shield thing")]
	public GameObject m_shield;
	[Tooltip("Time in seconds between the flashing of player when it's hit")]
	public float m_BlinkTime;
	[Tooltip("A reference to the particle system that indicates when the player is stunned")]
	public ParticleSystem m_StunnedParticles;
	[Tooltip("A reference to the particle system that plays when a stat is leveled up")]
	public ParticleSystem m_StatUpgradeParticles;
	[Tooltip("Distance in units the player dodges")]
	public float m_DodgeDist;
	[Tooltip("How quick the dodgeroll is")]
	public float m_DodgeSpeed;
	[Tooltip("The cooldown on the dodgeroll in Seconds")]
	public float m_DodgeDelay;
	[Tooltip("Damage of the players quick melee attack")]
	public float m_MeleeDamage;
	[Tooltip("How quick the melee attack is and how long it lasts for")]
	public float m_MeleeSpeed;
	[Tooltip("Melee attack cooldown in seconds")]
	public float m_MeleeDelay;
    [Tooltip("Players Starting Weapon")]
    public GameObject StartWeapon;
    [Header("Sound Files")]
    public AudioClip healthParticlesSFX;
    [Header("Foot Steps")]

    public AudioClip footstep1;
    public AudioClip footstep2;
    public AudioClip footstep3;
    public AudioClip footstep4;
    //==============================


    //==============================
    //Private Vars 

    //playersWeapon
    Rotate levelParticleCode;//UNCLEAR VAR
	Collider m_WeaponCol;                           //A reference to the weapons collider
	GameObject m_Weapon;                            //A reference to the currently equipped qeapon 
	Weapon m_WeaponScript;                          //A reference to the weapon script for the currently equipped weapon. 
	BoxCollider m_MeleeCollider;                    //A reference to the box collider used for the players melee attack.
	CharacterController m_RigidBody;                //A reference to the character controller
	Animator m_PlayerAnim;                          //A reference to the player's animator component
	ParticleSystem[] m_PlayerParticleSystems;       //A list of all particle systems on the player object or childed to the player object
	ParticleSystem m_HealthParticles;               //A reference to the particle system that plays when the player regains health
	ParticleSystem m_SlashParticles;                //A reference to the particle system that plays when the player attacks
	ParticleSystem m_HurtParticles;                 //A reference to the hurt particle system

	RandomizePlayer m_RandomizePlayer;              //Script used to give the player randomised appearance
	Image m_StaminaBar;                             //The Stamina bar in the UI
	List<MeshRenderer> m_Meshes;                    //An Array of all the mesh renderers for toggling pn and off when the play is hit
	SkinnedMeshRenderer m_meshSkin;                 //Refernce to the skin mesh renderer    

    double m_stunCounter;                           //The lenght of stun initial
    double m_stunPeriod;                            //lenght of stun remaining

    float m_Currency;                               //How much money the player has
	float m_UntouchableCooldown;                    //How long the player cant be hit for


	bool moving;                                    //Bool of whether or not the playing is moving
	bool m_Untouchable = false;                     //Bool whether or not the player can be hit
	bool sprinting = false;
	bool m_FreezePlayer = false;
	bool m_slowWalk = false;
	bool m_stunned = false;

	bool m_dodging = false;
	bool m_meleeing = false;
	bool m_readyForAction = true;
	bool shopOpen;

    float realStamina;
    int m_IsAttacking;                              //Whether or not the player is attacking ( 0 = false 1 = true) 
    //==============================

    Vector3 posLastFrame;
	float IdleWaitTime;
	float timeStationary;


	// Use this for initialization
	void Awake()
	{
        //Initialise Vars
		posLastFrame = Vector3.zero;
		IdleWaitTime = 0.2f;
		timeStationary = 0;
        moving = false;

        //Initialise vars that need to get components
        m_MainCamera = Camera.main;
        m_RigidBody = GetComponent<CharacterController>();
        m_RandomizePlayer = GetComponentInChildren<RandomizePlayer>();
        m_PlayerAnim = GetComponent<Animator>();
        m_MainCamera = Camera.main;
        m_Meshes = new List<MeshRenderer>();
        m_Meshes.AddRange(gameObject.GetComponentsInChildren<MeshRenderer>());
        m_meshSkin = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        levelParticleCode = GetComponentInChildren<Rotate>();
        levelParticleCode.gameObject.SetActive(false);

        //Set the players first weapon based on Public Prefab
        SetWeapon(Instantiate(StartWeapon));

        //dont destroy the player on scene changes
        DontDestroyOnLoad(gameObject);

        //Initialise particle systems and make sure they arent playing
        m_PlayerParticleSystems = GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem em in m_PlayerParticleSystems)
        {
            if (em.tag == ("HealthParticles"))
            {
                m_HealthParticles = em;
            }
            if (em.tag == "SlashParticles")
            {
                m_SlashParticles = em;
            }
            if (em.tag == "HurtParticles")
            {
                m_HurtParticles = em;
            }
        }
        m_HealthParticles.Clear();
        m_HealthParticles.Stop();

        //LEGACY CODE, Can be removed if we no longer want melee
        m_MeleeCollider = gameObject.GetComponent<BoxCollider>();
		m_MeleeCollider.enabled = false;
	}

	void OnLevelWasLoaded()
	{
		m_MainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}

    // Update is called once per frame
    void Update()
    {
        if (m_stunned)
            StunnedLoop();

        //Check input device when key pressed
        if (FetchKeyDown() != KeyCode.None)
            CheckInputDevice();

        //Update our invaunerability timer to see if we're invincible or not
        if (m_Untouchable)
            UpdateUntoucahble();

        //if the player is frozen, dont call any of the movement and attack updates

        if (!m_FreezePlayer)
        {
            //Updates the Players Movement
            MovementLoop();

            //this function is very unclear in what it does.
            confirmAttackState();

            //Advanced Movement
            DodgeLoop();

            //Updates the Players Animation
            PlayerAnimationLoop();
        }
    }

	public void SlowWalkSpeed(bool a_slow)
	{
		m_slowWalk = a_slow;
	}

	public void SetAttackState(int state)
	{
		m_IsAttacking = 0;
		m_PlayerAnim.SetBool("DoMelee", false);
	}

	public bool GetAttackState()
	{
		if (m_IsAttacking == 0)
			return false;
		else if (m_IsAttacking == 1)
			return true;
		else
			return false;
	}

	public void SetAnimationState(int state)
	{
		m_PlayerAnim.SetInteger("State", state);
	}

	public void RecieveStun(double a_duration)
	{
		m_StunnedParticles.Play();
		m_stunPeriod = a_duration;
		m_stunned = true;
	}


	//The set weapon function is used to assign a weapon on the player
	//inside setWeapon we will need to set which animator controller to use
	public void SetWeapon(GameObject a_weapon)
	{
		Weapon weaponScript = a_weapon.GetComponent<Weapon>();
		if (weaponScript)
		{
            if (m_Weapon)
            {
                m_Meshes[m_Meshes.IndexOf(m_Weapon.GetComponentInChildren<MeshRenderer>())].enabled = true;
                m_Meshes.Remove(m_Weapon.GetComponentInChildren<MeshRenderer>());
            }


			if (a_weapon.GetComponent<WeaponSwap>())
			{
				a_weapon.GetComponent<WeaponSwap>().enabled = false;
			}
			a_weapon.GetComponent<WeaponExperience>().enabled = true;
			if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
			{
				//this section needs new numbers 
				//need to get designers to find the hard code values
				m_PlayerAnim.SetInteger("WeaponType", 1);
				GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand"); //Find the hand to parent the weapons transform to. 
				a_weapon.transform.SetParent(weaponHand.transform);
				Vector3 transformPosition = new Vector3(-0.058f, -0.041f, -0.034f); //Hardcoded local position for the weapon
				Quaternion transformRotation;
				if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
				{
					transformRotation = Quaternion.Euler(new Vector3(278.239f, -39.682f, -179.682f)); //Hardcoded local rotation for the weapon
					if (m_WeaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE)
						m_SlashParticles.gameObject.transform.localPosition += new Vector3(-0.2f, 0, 0.5f);
				}
				else
				{
					transformRotation = Quaternion.Euler(new Vector3(176.661f, 42.81799f, -183.974f)); //Hardcoded local rotation for the weapon 
					if (m_WeaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
						m_SlashParticles.gameObject.transform.localPosition -= new Vector3(-0.2f, 0, 0.5f);
				}
				a_weapon.transform.localPosition = transformPosition;
				a_weapon.transform.localRotation = transformRotation;
				a_weapon.GetComponent<MeleeAttack>().enabled = true;
				//a_weapon.
				RemoveWeapon();
				m_Weapon = a_weapon;
				m_WeaponScript = m_Weapon.GetComponent<Weapon>();
				m_WeaponCol = m_Weapon.GetComponent<BoxCollider>();
			}
			else if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_RIFLE)
			{
				m_PlayerAnim.SetInteger("WeaponType", 3);
				GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand");
				a_weapon.transform.SetParent(weaponHand.transform);
				Vector3 transformPosition = new Vector3(-0.2258333f, 0.09711285f, -0.1330295f);
				Quaternion transformRotation = Quaternion.Euler(new Vector3(-1.407f, -129.509f, -3.848f));
				a_weapon.transform.localPosition = transformPosition;
				a_weapon.transform.localRotation = transformRotation;
				a_weapon.GetComponent<GunAttack>().enabled = true;

				RemoveWeapon();
				m_Weapon = a_weapon;
				m_WeaponScript = m_Weapon.GetComponent<Weapon>();
			}
			else if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
			{
				m_PlayerAnim.SetInteger("WeaponType", 2);
				GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand");
				a_weapon.transform.SetParent(weaponHand.transform);
				Vector3 transformPosition = new Vector3(-0.1112f, -0.0585f, -0.0248f);
				Quaternion transformRotation = Quaternion.Euler(new Vector3(-7.661f, -101.403f, -0.979f));
				a_weapon.transform.localPosition = transformPosition;
				a_weapon.transform.localRotation = transformRotation;
				a_weapon.GetComponent<GunAttack>().enabled = true;

				RemoveWeapon();
				m_Weapon = a_weapon;
				m_WeaponScript = m_Weapon.GetComponent<Weapon>();
			}
			else if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SHOTGUN)
			{
				m_PlayerAnim.SetInteger("WeaponType", 3);
				GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand");
				a_weapon.transform.SetParent(weaponHand.transform);
				Vector3 transformPosition = new Vector3(-0.03984392f, -0.03894692f, 0.0007825028f);
				Quaternion transformRotation = Quaternion.Euler(new Vector3(-1.407f, -129.509f, -3.848f));
				a_weapon.transform.localPosition = transformPosition;
				a_weapon.transform.localRotation = transformRotation;
				a_weapon.GetComponent<GunAttack>().enabled = true;

				RemoveWeapon();
				m_Weapon = a_weapon;
				m_WeaponScript = m_Weapon.GetComponent<Weapon>();
			}
			m_WeaponScript.UpdateUIStats();
            if (m_Weapon)
            { m_Meshes.Add(m_Weapon.GetComponentInChildren<MeshRenderer>()); }

		}
		m_Weapon = a_weapon;
	}

	//Destroys the currently equipped weapon
	public void RemoveWeapon()
	{
		GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().KillLevelUpUI();
		StopCoroutine("LevelUpUI");
		Destroy(m_Weapon);
		m_Weapon = null;
	}


	//Returns the players equipped weapon
	public GameObject GetWeapon()
	{
		return m_Weapon;
	}

	public bool GetIsStunned()
	{
		return m_stunned;
	}

	public void ColliderOn()
	{
		m_SlashParticles.Play();

		m_WeaponCol.enabled = true;
	}

	public void ColliderOff()
	{
		m_SlashParticles.Stop();

		m_WeaponCol.enabled = false;
	}

    public void PlaySFX(AudioClip a_Audio)
    {
        SoundManagerNew.instance.PlaySFX(a_Audio);
    }

    public void MuteSound()
	{
		SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
		sm.MuteSound(gameObject);
	}

	public void PlayHealthParticles()
	{
		PlaySFX(healthParticlesSFX);
		m_HealthParticles.Clear();
		m_HealthParticles.Play();
		GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().UpdatePlayerHealth(GetComponent<Health>().GetHealth(), GetComponent<Health>().GetMaxHealth());

	}

	//used to give the player money
	public void AddBank(float a_amount)
	{
		m_Currency += a_amount;
	}

	//used to take the players money, we could do this by just passing a negative number into the first function but come, who's gonna do that. 
	public void TakeBank(float a_amount)
	{
		m_Currency -= a_amount;
	}

	//returns the total amount of money the player has
	public float GetBank()
	{
		return m_Currency;
	}
	
    //used to gain XP
    //MARKED FOR REMOVAL
	public void GainExp(int a_experience)
	{
		if (m_Weapon)
			m_WeaponScript.weaponExp.GainEXP((int)(a_experience * GetComponent<PlayerStats>().GetXPBuff()));
	}

    //MARKED FOR REMOVAL
	public void LevelUpParticlesPlay()
	{
		levelParticleCode.gameObject.SetActive(false);
		levelParticleCode.gameObject.SetActive(true);
	}

	public void SetTransition()
	{
		m_PlayerAnim.SetInteger("WeaponType", 0);
	}

    //Freeze the players movement for a duration
    //Also sets the animation to idle
	public void PauseMovement(bool a_ShouldFreeze)
	{
        m_FreezePlayer = a_ShouldFreeze;
		SetAnimationState(0);
	}

	//Self contained DoDamage function which calls the health component to do damage then activates an invaunerable period. 
    //This causes the player to recieve damage
	public void DoDamage(float a_dmg)
	{
		if (!m_Untouchable)
		{
			GetComponent<Health>().DoDamage(a_dmg);
			GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().UpdatePlayerHealth(GetComponent<Health>().GetHealth(), GetComponent<Health>().GetMaxHealth());
			m_Untouchable = true;
			m_UntouchableCooldown = 0.0f;
			m_HurtParticles.Play();
			StartCoroutine(FlashPlayer());
		}
	}

    //UNCLEAR IF THIS IS BEST METHOD
    //LOOK INTO KEYCODE TO SEE IF GET KEY DOWN CAN JUST RETURN THE KEYCODE?
	KeyCode FetchKeyDown()
	{
		int e = 509;
		for (int i = 0; i < e; i++)
		{
			if (Input.GetKeyDown((KeyCode)i))
			{
				return (KeyCode)i;
			}
		}

		return KeyCode.None;
	}

    //UNCLEAR IF THIS IS BEST METHOD
    //LOOK INTO KEYCODE TO SEE IF GET KEY DOWN CAN JUST RETURN THE KEY?
    KeyCode FetchKey()
	{
		int e = 509;
		for (int i = 0; i < e; i++)
		{
			if (Input.GetKey((KeyCode)i))
			{
				return (KeyCode)i;
			}
		}

		return KeyCode.None;
	}

    //cancles reloading animation
	public void SetReloadOff()
	{
		GetComponent<Animator>().SetBool("Reloading", false);
	}

    //MARKED FOR REMOVAL
	public void PlayStatUpgradeParticles()
	{
		if (m_StatUpgradeParticles != null)
			m_StatUpgradeParticles.Play();
	}

    //turns off player invincibility
	public void TurnOffInvincible()
	{
		m_Untouchable = false;
	}

    //Co-routine function that causes the player to flash when the player can no longer take damage
	IEnumerator FlashPlayer()
	{
		foreach (MeshRenderer mesh in m_Meshes)
		{
			mesh.enabled = false;
			m_meshSkin.enabled = false;
		}
		while (m_Untouchable)
		{
			foreach (MeshRenderer mesh in m_Meshes)
			{
				mesh.enabled = !mesh.enabled;
			}
			m_meshSkin.enabled = !m_meshSkin.enabled;
			yield return new WaitForSecondsRealtime(m_BlinkTime);
		}
		foreach (MeshRenderer mesh in m_Meshes)
		{
			mesh.enabled = true;
			m_meshSkin.enabled = true;
		}
	}

	IEnumerator Dodge()
	{
		m_dodging = true;
		m_readyForAction = false;
		float count = 0.0f;
		Vector3 forward = transform.forward;
		while (count < m_DodgeDist)
		{
			m_RigidBody.Move(forward * m_DodgeSpeed * Time.deltaTime);
			count += m_DodgeSpeed * Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		m_dodging = false;
		yield return new WaitForSecondsRealtime(m_DodgeDelay);
		m_readyForAction = true;
	}

	IEnumerator Melee()
	{
		m_meleeing = true;
		m_readyForAction = false;
		m_MeleeCollider.enabled = true;
		//do melee anim stuff
		yield return new WaitForSecondsRealtime(1 / m_MeleeSpeed);

		m_MeleeCollider.enabled = false;
		m_meleeing = false;

		yield return new WaitForSecondsRealtime(m_MeleeDelay);
		m_readyForAction = true;
	}

	//Melee Attack actions
	void OnTriggerEnter(Collider col)
	{
		if (m_MeleeCollider.enabled)
		{
			if (col.gameObject.tag == "Enemy")
			{

				EnemyAI ai = col.gameObject.GetComponent<EnemyAI>();
				if (ai)
					ai.TakeDamage(m_MeleeDamage);
				else
					col.gameObject.GetComponent<Health>().DoDamage(m_MeleeDamage);
				Vector3 dist = gameObject.transform.position - col.gameObject.transform.position;
				Vector3 forceVector = dist.normalized * 10;
				forceVector.y = 0;
				Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();
				rb.isKinematic = false;
				rb.AddForce(-forceVector, ForceMode.Impulse);
				col.gameObject.GetComponentInChildren<Canvas>(true).enabled = true;
			}

			else if (col.gameObject.tag == "Boss")
			{
				BossAI ai = col.gameObject.GetComponent<BossAI>();
				if (ai)
					ai.TakeDamage(m_MeleeDamage);
			}
		}
	}

	public void shopChanged(bool state)
	{
		shopOpen = state;
	}

	void AttackLoop()
	{

	}

    void DodgeLoop()
    {
        if (Input.GetButton("Dodge") && m_readyForAction)
            StartCoroutine(Dodge());
        if (Input.GetButton("Melee") && m_readyForAction)
            StartCoroutine(Melee());
    }

    void confirmAttackState()
    {
        if (!m_dodging && !shopOpen)
        {
            if (Input.GetAxisRaw("Fire1") > 0)
            {
                m_IsAttacking = 1;
            }
            if (Input.GetAxisRaw("Fire1") == 0 && m_Weapon.GetComponent<GunAttack>())
                m_IsAttacking = 0;
            if (Input.GetAxisRaw("Fire1") == 0 && m_Weapon.GetComponent<MeleeAttack>() && !m_PlayerAnim.GetBool("DoMelee"))
                m_IsAttacking = 0;
        }
    }

    void UpdateUntoucahble()
    {
        if ((m_UntouchableCooldown += Time.deltaTime) >= m_UntouchableTime)
        {
            m_Untouchable = false;
        }
    }

    void CheckInputDevice()
    {
        KeyCode curr = FetchKeyDown();
        if ((int)curr >= 330 && (int)curr < 350)
            m_inputDevice = 2;
        else
            m_inputDevice = 1;
    }

    float UpdateSpeed()
    {
       return m_slowWalk ? m_MoveSpeed / m_speedModifierDenominator : m_MoveSpeed;
    }

	//Manages when the player is stunned, does not stun the player
	void StunnedLoop()
	{
		if (m_stunned)
		{
			m_stunCounter += Time.deltaTime;
			if (m_stunCounter >= m_stunPeriod)
			{
				m_StunnedParticles.Stop();
				m_stunCounter = 0.0f;
				m_stunPeriod = 0.0f;
				m_stunned = false;
			}
			return;
		}
	}

    void PlayerAnimationLoop()
    {
        Vector3 velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        Vector3 forwardVec = transform.forward;
        float directionTravelling = Vector3.Angle(forwardVec, velocity.normalized);


        //set the directions to an easier to understand rotation
        directionTravelling += 45;
        directionTravelling %= 360;

        //if animations stuff up again, it's probably this if statements fault
        if (timeStationary > IdleWaitTime)
        {
            m_PlayerAnim.SetInteger("State", 0);
        }

        else
        {
            {
                if (0 < directionTravelling && directionTravelling <= 90)
                {
                    //forwards run
                    m_PlayerAnim.SetInteger("State", 1);
                }
                else if (90 < directionTravelling && directionTravelling <= 180)
                {
                    if (Vector3.Dot(forwardVec, velocity.normalized) < 0)
                    {
                        //right run
                        m_PlayerAnim.SetInteger("State", 3);
                    }
                    else if (Vector3.Dot(forwardVec, velocity.normalized) > 0)
                    {
                        //left run
                        m_PlayerAnim.SetInteger("State", 5);
                    }
                }
                else if (180 < directionTravelling && directionTravelling <= 270)
                {
                    //backwards run
                    m_PlayerAnim.SetInteger("State", 4);
                }
                else
                {
                    m_PlayerAnim.SetInteger("State", 1);
                }
            }
        }
    }

    void MovementLoop()
    {
        float modMoveSpeed = UpdateSpeed();

        Vector3 velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        if (velocity.magnitude != 0)
        {
            moving = true;
            velocity.Normalize();
        }
        else
        {
            moving = false;
        }

        if (!m_dodging)
            m_RigidBody.Move((velocity * modMoveSpeed * Time.deltaTime));

        m_PlayerAnim.SetFloat("WalkSpeed", modMoveSpeed * 0.15f);

        if (m_inputDevice == 1 && m_Weapon)
        {
            //create a ray and a plane to cast it against
            Ray ray = m_MainCamera.ScreenPointToRay(Input.mousePosition);
            Plane ground = new Plane(Vector3.up, new Vector3(0, m_Weapon.transform.position.y, 0));
            float mouseDistance;
            //1.328097f

            //Do the raycast, if hit, use the returned distance to find the point the mouse is over. 
            if (ground.Raycast(ray, out mouseDistance) && Time.timeScale != 0)
            {
                //Detect mouse moving
                Vector3 point = ray.GetPoint(mouseDistance);
                transform.LookAt(new Vector3(point.x, transform.position.y, point.z));
            }
        }
        else
        {
            Vector2 rightStick = new Vector2(Input.GetAxis("AimX"), Input.GetAxis("AimY"));
            transform.LookAt(new Vector3(transform.position.x + rightStick.x, transform.position.y, transform.position.z + rightStick.y));
        }

        if (velocity == Vector3.zero)
        {
            timeStationary += Time.deltaTime;
        }
        else
        {
            timeStationary = 0;
        }
        if (!m_RigidBody.isGrounded)
            m_RigidBody.Move(Physics.gravity);
    }

    //USED TO PLAY FOOTSTEP SOUNDS
    //INCORRECTLY NAMED AS THESE ARE ANIMATION EVENTS AND RENAMING IS A PAIN!!!
    //Although this still takes an index, incase designers wish to swap back to designated sound
    //it does not use the parameter and instead plays a random footstep sound
    public void PlaySound(int soundIndex)
    {
        int randIndex = Random.Range(0, 4);
        switch (randIndex)
        { 
            case 0:
            SoundManagerNew.instance.PlayFootSteps(footstep1);
            break;
        case 1:
                SoundManagerNew.instance.PlayFootSteps(footstep2);
            break;
        case 2:
                SoundManagerNew.instance.PlayFootSteps(footstep3);
            break;
        case 3:
                SoundManagerNew.instance.PlayFootSteps(footstep4);
            break;
        default:
            break;
        }
    }
}
