﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    public int SpeedOfRotation;
    GameObject Player;
    float lastFramex;
    float lastFramez;

    ParticleSystem[] m_System;
    ParticleSystem.Particle[] m_Particles;
    public float m_Drift = 0.01f;
    // Use this for initialization
    void Awake()
    {
       Player = GameObject.FindGameObjectWithTag("Player");
       lastFramex = Player.transform.position.x;
       lastFramez = Player.transform.position.z;

    }

    void OnEnable()
    {

    }
	// Update is called once per frame
	void Update () {
        Vector3 Temp = gameObject.transform.position;
        float DifX = lastFramex - Player.transform.position.x;
        float Difz = lastFramez - Player.transform.position.z;

        lastFramex = Player.transform.position.x;
        lastFramez = Player.transform.position.z;

        Temp.x = Player.transform.position.x;
        gameObject.transform.position = Temp;      
        gameObject.transform.Rotate(0,0, SpeedOfRotation);
        InitializeIfNeeded();

        // GetParticles is allocation free because we reuse the m_Particles buffer between updates
        for (int j = 0; j < 4; ++j)
        {
            m_Particles = new ParticleSystem.Particle[m_System[j].maxParticles];

            int numParticlesAlive = m_System[j].GetParticles(m_Particles);

            // Change only the particles that are alive
            for (int i = 0; i < numParticlesAlive; i++)
            {
                m_Particles[i].position = new Vector3(m_Particles[i].position.x - DifX, m_Particles[i].position.y, m_Particles[i].position.z - Difz);
            }

            // Apply the particle changes to the particle system
            m_System[j].SetParticles(m_Particles, numParticlesAlive);
        }
    }

    void InitializeIfNeeded()
    {
        
            if (m_System == null)
                m_System = GetComponentsInChildren<ParticleSystem>();
        
        //if (m_Particles == null || m_Particles.Length < m_System.maxParticles)
            //m_Particles = new ParticleSystem.Particle[m_System.maxParticles];
    }



}
