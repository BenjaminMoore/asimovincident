﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Bullet : MonoBehaviour
{

    public GameObject trailPrefab;
    private GameObject[] bulletTrail;

    private float m_damage;
    private ParticleSystem m_system;
    private List<ParticleCollisionEvent> m_colEvents;
    // Use this for initialization
    void Awake()
    {
        m_damage = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Weapon>().m_damage;
        m_system = GetComponent<ParticleSystem>();
        m_colEvents = new List<ParticleCollisionEvent>(1);
        bulletTrail = new GameObject[m_system.maxParticles];
    }

    // Update is called once per frame
    void Update()
    {
        ParticleSystem.Particle[] part = new ParticleSystem.Particle[m_system.particleCount];
        int na = m_system.GetParticles(part);
        for (int i = 0; i < na; ++i)
        {
            if(bulletTrail[i] == null)
            {
                bulletTrail[i] = Instantiate(trailPrefab);
            }
            bulletTrail[i].transform.position = part[i].position;
        }
        if (!m_system.IsAlive())
        {
            foreach (GameObject g in bulletTrail)
            {
                Destroy(g);
            }
            Destroy(gameObject);
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.tag == "Player")
            return;
        int len = m_system.GetSafeCollisionEventSize();
        if (m_colEvents.Capacity < len)
            m_colEvents = new List<ParticleCollisionEvent>(len);

        int numColEvents = m_system.GetCollisionEvents(other, m_colEvents);
        foreach(ParticleCollisionEvent eve in m_colEvents)
        {
            if (other.tag == "Enemy")
            {
                other.GetComponent<EnemyAI>().TakeDamage(m_damage);
                continue;
            }
            else if (other.tag == "Boss")
            {
                other.GetComponent<BossAI>().TakeDamage(m_damage);
                continue; 
            }
        }
        foreach (GameObject g in bulletTrail)
        {
            Destroy(g);
        }
        Destroy(gameObject);
    }
}
