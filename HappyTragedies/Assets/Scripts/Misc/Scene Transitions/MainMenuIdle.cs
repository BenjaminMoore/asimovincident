﻿using UnityEngine;
using UnityEngine.SceneManagement; 
using System.Collections;

public class MainMenuIdle : MonoBehaviour {

    //=====================
    //Public Vars
    public float m_IdleTime;
    public int m_SceneToLoadIfIdle;
    //=====================

    private float  m_counter; 

	// Use this for initialization
	void Start () {
        m_counter = 0;
    } 
	
	// Update is called once per frame
	void Update () {
        m_counter += Time.deltaTime;

        if (Input.anyKeyDown)
            m_counter = 0.0f;
        else if (m_counter >= m_IdleTime)
        {
            SceneManager.LoadScene(m_SceneToLoadIfIdle);
            Destroy(GameObject.FindGameObjectWithTag("SoundManager"));
        }
	}
}
