﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopNameHandler : MonoBehaviour {

    [Tooltip("Where the name is in the UI heirachy, 0 indexed (Begins at 0)")]
    public int nameNumber;
    string[] WepNamesTemp;

    //private vars
    private ShopFunctions shopfunctions;
    Text gameText;

    private void Awake()
    {
        gameText = GetComponent<Text>();
        WepNamesTemp = new string[5];
        WepNamesTemp[0] = "Ragnarok";
        WepNamesTemp[1] = "Limb Wrecker";
        WepNamesTemp[2] = "Alien Blaster";
        WepNamesTemp[3] = " Deaths Wish";
        WepNamesTemp[4] = "Hades";
    }

    void Start()
    {
        shopfunctions = GetComponentInParent<ShopFunctions>();
    }

    public void wepPurchased()
    {
        gameText.text = "SOLD";
    }

    public void GenerateName()
    {
        string nameChosen;
        int randNum = Random.Range(0, 4);
        nameChosen = WepNamesTemp[randNum];
        gameText.text = nameChosen;
    }
}
