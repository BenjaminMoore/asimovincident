﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR(s): Douglas Landers & Matt Callahan 
/// Last Editted: 9th July 2017 by Douglas Landers 
/// The dying default behaviour is the generally used behaviour for the death of enemes 
/// This behaviour will invoke a screenshake using the FollowPlayer script on the camera controller
/// and also turn of the scripts which handle interactions with this object such as collisiom and skinned renderers
/// </summary>
/// 
public class DyingDefaultBehaviour : AIBehaviour {

    //===================
    //Private Vars
    Health                      m_Health;              //A reference to this objects health component 
    EnemyAI                     m_AI;                  //A reference to this objects EnemyAI component 
    Rigidbody                   m_RigidBody;           //a reference to this objects rigidbody
    FollowPlayer                m_CameraScript;        //a reference to the FollowPlayer script on the Main camera, used to invoke a screenshake
    CapsuleCollider             m_Collider;            //This Objects CapsuleCollider, used for all collision
    PlayerController            m_Player;              //A reference to the player controller
    UnityEngine.AI.NavMeshAgent m_Agent;               //A reference to the navmeshagent used to control steering for this enemy
    float   m_KillReward;                              //The amount of entrials this object will reward to the player on death
    int     m_ExpReward;                               //The amount of EXP this object will reward to the player on death
    //===================

   //Retrieve any required references or variables 
    public override eResult Initialise()
    {
        if (m_Health == null)
            m_Health = GetComponent<Health>();
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_RigidBody == null)
            m_RigidBody = GetComponent<Rigidbody>();
        if (m_Collider == null)
            m_Collider = GetComponent<CapsuleCollider>();
        if (m_CameraScript == null)
            m_CameraScript = Camera.main.GetComponent<FollowPlayer>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        if (m_Agent == null)
            m_Agent = m_AI.GetComponent<UnityEngine.AI.NavMeshAgent>(); 
        if (m_KillReward <= 0.0f)
            m_KillReward = m_AI.GetKillReward();
        if (m_ExpReward <= 0.0f)
            m_ExpReward = m_AI.GetExpReward();

        if (m_Health && m_AI && m_RigidBody && m_Collider && m_CameraScript && m_Player
            && m_Agent && m_KillReward != 0.0f && m_ExpReward != 0.0f)
            return eResult.Success;
        else return eResult.Fail; 
    }

    public override eResult Execute()
    {
        //Invoke death for the enemy, updating its animation, explosion and action state
        m_AI.SetAnimationBool("Dead", true);
        m_AI.SetAnimationFloat("Speed", m_Agent.velocity.magnitude); 
        m_AI.SetActionState(3);
        m_AI.m_deathXplode.Play();

       

        if(!m_CameraScript.Shaking()) //Invoke the screenshake in the camera script
            m_CameraScript.Shake(m_AI.m_XplodeShakeDuration, m_AI.m_XplodeShakeIntensity);

        //De-activate any components that will interfere with the enemy being 'dead' 
        m_Agent.enabled = false;
        m_Collider.enabled = false;
        m_RigidBody.freezeRotation = true;
        m_RigidBody.useGravity = false;
        m_RigidBody.constraints = RigidbodyConstraints.FreezeAll;

        //Give the player some exp and money. 
        //m_Player.GainExp(m_ExpReward);
        m_Player.AddBank(m_KillReward);

        //****MARK FOR DELETE*****************                                                 // 
        //Drop a weapon if one hasn't been dropped before                                      //
      // if (m_AI.GetRoom() && !m_AI.GetRoom().hasSpawnedWep)                                   //
      // {                                                                                      //
      //     WeaponGenerator wpGen = FindObjectOfType<WeaponGenerator>();                       //
      //     GameObject instance = wpGen.GenerateWeapon(0);   //Generate the weapon           //
      //     instance.GetComponent<WeaponSwap>().enabled = true;                                //
      //                                                                                        //
      //     m_AI.GetRoom().hasSpawnedWep = true;                                               //
      //     instance.transform.position = gameObject.transform.position + new Vector3(0, 0.2f);//
      //     if (instance.GetComponent<GunAttack>())                                            //
      //     {                                                                                  //
      //         instance.GetComponent<GunAttack>().enabled = false;                            //
      //     }                                                                                  //
      //     else                                                                               //
      //     {                                                                                  //
      //         instance.GetComponent<MeleeAttack>().enabled = false;                          //
      //     }                                                                                  //
      // }                                                                                      //
        //********END MARK FOR DELETE*************                                             //

        return eResult.Success; 
    }

    //Always Ready to complete
    public override eResult Evaluate()
    {
        return eResult.Success; 
    }

    //This function is called by the animator at the end of the death animation
    public void FinishedDying()
    {
        m_AI.SetAnimationBool("FinishedDead", true); 
        m_AI.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false; //Turn off the mesh renderer
        gameObject.SetActive(false);
    }
}
