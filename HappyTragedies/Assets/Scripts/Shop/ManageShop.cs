﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageShop : MonoBehaviour {




    //Private Vars
    bool m_IsPlayerClose = false;
    float availableCurrency;
    bool gunActive;
    GameObject shop;
    GameObject toolTip;
    PlayerController Player;

    private void Awake()
    {
        //get components in awake to ensure they are active
        toolTip = GetComponentInChildren<InteractionShopUI>().gameObject;
        shop = GetComponentInChildren<ShopFunctions>().gameObject;
    }

    void Start ()
    {
        //initially have UI inactive, activated from other scripts when player is nearby
        shop.SetActive(false);
        toolTip.SetActive(false);
        Player = FindObjectOfType<PlayerController>();
    }

    //given a set input, determine if we should open the shop
    void Update()
    {
        if (m_IsPlayerClose && Input.GetButtonDown("Swap"))
        {
            if (shop.activeSelf == false)
                OpenShop();
            else
                CloseShop();
        }
    }


    //open the ui when the player is close enough
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            m_IsPlayerClose = true;
            toolTip.SetActive(true);
        }
    }


    void OpenShop()
    {
        checkActiveWeaponType();
        if (gunActive)
        {
            Player.gameObject.GetComponentInChildren<GunAttack>().enabled = false;
        }
        else
            Player.gameObject.GetComponentInChildren<MeleeAttack>().enabled = false;
        Player.shopChanged(true);
        shop.SetActive(true);
        Debug.Log("Shop Opened");
        toolTip.SetActive(false);
    }

    public void CloseShop()
    {
        Player.shopChanged(false);
        checkActiveWeaponType();
        if(gunActive)
        {
            Player.gameObject.GetComponentInChildren<GunAttack>().enabled = true;
        }
        else
            Player.gameObject.GetComponentInChildren<MeleeAttack>().enabled = true;
        shop.SetActive(false);
        if(m_IsPlayerClose)
        {
            toolTip.SetActive(true);
        }
    }

    //close the ui when the player moves too far away
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            m_IsPlayerClose = false;
            CloseShop();
            toolTip.SetActive(false);
        }

    }

    void checkActiveWeaponType()
    {
        if (Player.gameObject.GetComponentInChildren<MeleeAttack>() != null)
            gunActive = false;
        else if (Player.gameObject.GetComponentInChildren<GunAttack>() != null)
            gunActive = true;
        else
            Debug.Log("No weapon Attack Script");
    }

    public void setCostUI()
    {
        shop.GetComponent<ShopFunctions>().setCostUI();
    }
    public void setNameUI()
    {
        shop.GetComponent<ShopFunctions>().SetNameUI();
    }
}
